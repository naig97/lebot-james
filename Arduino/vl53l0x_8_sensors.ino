// from https://forum.pololu.com/t/vl53l0x-maximum-sensors-on-i2c-arduino-bus/10845/7?u=oleglyan

#include <Wire.h>
#include <VL53L0X.h>

//#define XSHUT_pin1 2 // not required for address change
#define XSHUT_pin2 3
#define XSHUT_pin3 4
#define XSHUT_pin4 5
#define XSHUT_pin5 6
#define XSHUT_pin6 7
#define XSHUT_pin7 8
#define XSHUT_pin8 9

//ADDRESS_DEFAULT 0b0101001 or 41
#define Sensor1_newAddress 1
#define Sensor2_newAddress 2
#define Sensor3_newAddress 3
#define Sensor4_newAddress 4
#define Sensor5_newAddress 5
#define Sensor6_newAddress 6
#define Sensor7_newAddress 7
#define Sensor8_newAddress 8


VL53L0X Sensor1;
VL53L0X Sensor2;
VL53L0X Sensor3;
VL53L0X Sensor4;
VL53L0X Sensor5;
VL53L0X Sensor6;
VL53L0X Sensor7;
VL53L0X Sensor8;

int latestValues[9];
int incomingByte = 99;

void setup()
{ /*WARNING*/
  //Shutdown pins of VL53L0X ACTIVE-LOW-ONLY NO TOLERANT TO 5V will fry them
//pinMode(XSHUT_pin1, OUTPUT);
  pinMode(XSHUT_pin2, OUTPUT);
  pinMode(XSHUT_pin3, OUTPUT);
  pinMode(XSHUT_pin4, OUTPUT);
  pinMode(XSHUT_pin5, OUTPUT);
  pinMode(XSHUT_pin6, OUTPUT);
  pinMode(XSHUT_pin7, OUTPUT);
  pinMode(XSHUT_pin8, OUTPUT);
  
  Serial.begin(9600);
  
  Wire.begin();
  //Change address of sensor and power up next one
  Sensor8.setAddress(Sensor8_newAddress);
  pinMode(XSHUT_pin8, INPUT);
  delay(10);
  
  Sensor7.setAddress(Sensor7_newAddress);
  pinMode(XSHUT_pin7, INPUT);
  delay(10);
  
  Sensor6.setAddress(Sensor6_newAddress);
  pinMode(XSHUT_pin6, INPUT);
  delay(10); //For power-up procedure t-boot max 1.2ms "Datasheet: 2.9 Power sequence"
  
  Sensor5.setAddress(Sensor5_newAddress);
  pinMode(XSHUT_pin5, INPUT);
  delay(10);
  
  Sensor4.setAddress(Sensor4_newAddress);
  pinMode(XSHUT_pin4, INPUT);
  delay(10);
  
  Sensor3.setAddress(Sensor3_newAddress);
  pinMode(XSHUT_pin3, INPUT);
  delay(10);
  
  Sensor2.setAddress(Sensor2_newAddress);
  pinMode(XSHUT_pin2, INPUT);
  delay(10);

 /* Sensor1.setAddress(Sensor1_newAddress);
  pinMode(XSHUT_pin1, INPUT);
  delay(10);
*/
  
  Sensor1.init();
  Sensor2.init();
  Sensor3.init();
  Sensor4.init();
  Sensor5.init();
  Sensor6.init();
  Sensor7.init();
  Sensor8.init();


  Sensor1.setMeasurementTimingBudget(45000);
  Sensor2.setMeasurementTimingBudget(45000);
  Sensor3.setMeasurementTimingBudget(45000);
  Sensor4.setMeasurementTimingBudget(45000);
  Sensor5.setMeasurementTimingBudget(45000);
  Sensor6.setMeasurementTimingBudget(45000);
  Sensor7.setMeasurementTimingBudget(45000);
  Sensor8.setMeasurementTimingBudget(45000);
  
  Sensor1.setTimeout(500);
  Sensor2.setTimeout(500);
  Sensor3.setTimeout(500);
  Sensor4.setTimeout(500);
  Sensor5.setTimeout(500);
  Sensor6.setTimeout(500);
  Sensor7.setTimeout(500);
  Sensor8.setTimeout(500);

  // Start continuous back-to-back mode (take readings as
  // fast as possible).  To use continuous timed mode
  // instead, provide a desired inter-measurement period in
  // ms (e.g. sensor.startContinuous(100)).
  Sensor1.startContinuous();
  Sensor2.startContinuous();
  Sensor3.startContinuous();
  Sensor4.startContinuous();
  Sensor5.startContinuous();
  Sensor6.startContinuous();
  Sensor7.startContinuous();
  Sensor8.startContinuous();
}

void loop()
{
  if(Serial.available() > 0) {
    incomingByte = Serial.read();
    if (incomingByte != 99) {
      int s = latestValues[incomingByte];
      byte dist[3];
      dist[0] = s/127;
      dist[1] = s%127;
      dist[2] = -2;
      Serial.write(dist, 3);
      incomingByte = 99;
    }
  }

  latestValues[1] = Sensor1.readRangeContinuousMillimeters();
  latestValues[2] = Sensor2.readRangeContinuousMillimeters();
  latestValues[3] = Sensor3.readRangeContinuousMillimeters();
  latestValues[4] = Sensor4.readRangeContinuousMillimeters();
  latestValues[5] = Sensor5.readRangeContinuousMillimeters();
  latestValues[6] = Sensor6.readRangeContinuousMillimeters();
  latestValues[7] = Sensor7.readRangeContinuousMillimeters();
  latestValues[8] = Sensor8.readRangeContinuousMillimeters();
}
