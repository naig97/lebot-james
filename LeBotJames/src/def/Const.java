package def;

import ch.ntb.inf.deep.runtime.mpc555.driver.TPU_PWM;

public class Const {

	// Constants for PID
	public static final float TS = 0.001f;					// task period [s]
	public static final float KP = 1;						// controller gain factor
	public static final float TN = 0.006f;					// time constant of the controller (equal to the mechanical time constant of the connected DC motor)
	public static final float MAX_SPEED = 20;				// maximum speed [1/s]
	public static final double PID_K_PROPORTIONAL = 0.8d;
	public static final double PID_K_INTEGRAL = 0.3d;
	public static final float WHEEL_DIAMETER = 53.4f;
	public static final float WHEEL_CIRCUMFERENCE = 168f;
	public static final float GEAR_DIAMETER = 23;
	
	// Constants for tasks
	public static final int MOVE_TASK_PERIOD 			= 10;
	public static final int ENCODER_TASK_PERIOD 		= 10;
	public static final int TASK_PERIOD 				= (int)(TS * 1000); // 1ms
	public static final int SPEED_CONTROL_TASK_PERIOD 	= (int)(TS * 1000); // 1ms
	public static final int MAIN_TASK_PERIOD 			= 10; // 10ms
	public static final int IR_TASK_PERIOD				= 50;
	
	// Constants for Sensors
	public static final boolean USE_QADCA  = true;

	public static final int SENSOR_FRONT_L = 1;
	public static final int SENSOR_FRONT_R = 2;
	public static final int SENSOR_BACK_L  = 3;
	public static final int SENSOR_BACK_R  = 4;
	public static final int SENSOR_RIGHT_F = 5;
	public static final int SENSOR_RIGHT_B = 6;
	public static final int SENSOR_LEFT_F  = 7;
	public static final int SENSOR_LEFT_B  = 8;

	public static final int SENSOR_VAL_DIF_B_W = 12;
	public static final int SENSOR_VAL_DIF_W_B = -12;
	
	// Constants for State Machine
	
	// Constants for Encoder
	public static final boolean USE_TPUA = true;
	public static final boolean USE_TPUB = false;
	public static final int TICKS_PER_ROTATION = 1024;			// impulse/ticks per rotation of the encoder.
	
	// Constants for Motor
	public static final boolean vorwaerts = false;
	public static final boolean rueckwaerts = true;
	
	public static final float MOTOR_VOLTAGE = 9f;				// maximum voltage [V]
	public static final int MOTOR_DRIVE_GEAR_TRANS_RATIO = 86;
	public static final int MOTOR_SHOOT_GEAR_TRANS_RATIO = 43;
	
	// Constants for Drive System
	public static final float DEGREE_FOR_90DEG				= 4.537856f;
	public static final float TURN_VELOCITY					= 100f;
	public static final float P_FAKTOR_TURN					= 60f;
	public static final float ANGLE_ERROR_TURN				= 0.0001f;
	public static final float MAX_DRIVE_SPEED				= 70f;
	
	// Constants for Position Distances
	public static final int P_FAKTOR_DISTANCE	= 10;
	public static final int	DIST_POS_SIDE		= 70;
	
	// Constants for Shoot System
	public static final float POSITION_ZERO 				= 0f;
	public static final float POSITION_GET_BALL 			= -2.868687f; // Wert Ermittelt am 20.05.2019 fs
	public static final float POSITION_SHOOT_VERTICAL 		= -2.396626f; // Wert Ermittelt am 29.05.2019 fs
	public static final float POSITION_SHOOT_HORIZONTAL 	= -2.179064f; // Wert Ermittelt am 29.05.2019 fs
	public static final float POSITION_SHOOT_HORIZONTAL2 	= -2.259064f; // Wert Ermittelt am 29.05.2019 fs
	public static final float POSITION_SHOOT_BASKET		 	= -2.776043f; // Wert Ermittelt am 29.05.2019 fs
	
	public static final float VELOCITY_SHOOTSYSTEM			= 60f;
	public static final float P_FAKTOR_SHOOTSYSTEM			= 50f;
	public static final float ANGLE_ERROR					= 0.0001f;
	public static final int	  MAGNET_ON_TIME				= 300; // 300ms ein fuer sauberes Ausloesen
	
	// ------------------------------------
	public static final int PWM_PERIOD = 50000 / TPU_PWM.tpuTimeBase; // =62
	public static final int AKKU_BAD_VALUE = 900; // entspricht 9.9V
	
	
	// Communication Table
	//--------------------
	
	// Misc
	public static final int START				= 100;
	public static final int CONNECTION_CHECK	= 111;
	public static final int CONNECTION_OK 		= 155;
	public static final int MISUNDERSTOOD		= 122;
	public static final int REPEAT 				= 133;
	public static final int BALL_FALSE			= 144;
	public static final int BALL_TRUE			= 166;
	
	// Position Change 
	public static final int GOTO_POS2	= 2002;
	public static final int GOTO_POS3	= 2003;
	public static final int GOTO_POS4	= 2004;
	public static final int GOTO_POS5	= 2005;
	public static final int GOTO_POS6	= 2006;
	public static final int GOTO_POS7	= 2007;
	public static final int GOTO_POS8	= 2008;
	public static final int GOTO_POS9	= 2009;
	
	// Ready for Catch
	public static final int READY_POS1	= 3001;
	public static final int READY_POS2	= 3002;
	public static final int READY_POS3	= 3003;
	public static final int READY_POS4	= 3004;
	public static final int READY_POS5	= 3005;
	public static final int READY_POS6	= 3006;
	public static final int READY_POS7	= 3007;
	public static final int READY_POS8	= 3010;
	public static final int READY_POS9	= 3009;
	
	// Shoot
	public static final int SHOOT_POS1	= 4001;
	public static final int SHOOT_POS2	= 4002;
	public static final int SHOOT_POS3	= 4003;
	public static final int SHOOT_POS4	= 4004;
	public static final int SHOOT_POS5	= 4005;
	public static final int SHOOT_POS6	= 4006;
	public static final int SHOOT_POS7	= 4007;
	public static final int SHOOT_POS8	= 4008;
	public static final int SHOOT_POS9	= 4009;
	public static final int SHOOT_FINAL	= 4010;
	
	private Const() {}; // private constructor to prevent instantiation
}
