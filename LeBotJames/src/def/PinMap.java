package def;

public class PinMap {
	
	// PINs for motor encoders
	public static final int MOTOR_LEFT_ENCODER_A = 0;
	public static final int MOTOR_LEFT_ENCODER_B = 1;
	public static final int MOTOR_RIGHT_ENCODER_A = 2;
	public static final int MOTOR_RIGHT_ENCODER_B = 3;
	public static final int MOTOR_SHOOT_ENCODER_A = 4;
	public static final int MOTOR_SHOOT_ENCODER_B = 5;
	
	// PINs for motors PWM
	public static final int MOTOR_LEFT_PWM = 6;
	public static final int MOTOR_RIGHT_PWM = 7;
	public static final int MOTOR_SHOOT_PWM = 8;
	
	// PINs for debug LEDs
	public static final int DEBUG_LED1 = 9;
	public static final int DEBUG_LED2 = 10;
	public static final int DEBUG_LED3 = 11;
	
	// PINs for debug switches
	public static final int DEBUG_SWITCH1 = 12;
	public static final int DEBUG_SWITCH2 = 13;
	
	// PINs for analog inputs
	public static final int ADC_BATTERY = 0;
	public static final int ADC_GROUND_SENSORS = 1;
	public static final int ADC_BALL_SENSOR = 2;
	public static final int ADC_VELOCITY = 3;
	
	// PINs for status LEDs
	public static final int LED_STATUS_WIFI = 13;
	public static final int LED_STATUS_BATTERY_BAD = 14;
	public static final int LED_STATUS_MANUAL_MODE_ON = 11;
	
	// PINs for ground sensors
	public static final int GROUND_SENSOR_ENABLE = 10;
	public static final int GROUND_SENSOR_B = 9;
	public static final int GROUND_SENSOR_A = 8;
	
	// PINs for manual modus switches
	public static final int MANUAL_SWITCH_DIRECTION = 7;
	public static final int MANUAL_SWITCH_DIRECTION_SET = 6;
	
	// PINs for motor divers
	public static final int DIRECTION_MOTOR_SHOOT = 5;
	public static final int DIRECTION_MOTOR_RIGHT = 0;
	public static final int DIRECTION_MOTOR_LEFT = 1;
	
	// PINs for various outputs
	public static final int LIFTING_MAGNET = 2;
	public static final int CHARGE_CONDENSATOR = 4;
	public static final int ELECTRIC_MAGNET = 3;
	
	// PINs serial communication
	public static final int ARDUINO_TX = 1;
	public static final int WIFI_TX = 2;
	public static final int ARDUINO_RX = 1;
	public static final int WIFI_RX = 2;
	public static final int WIFI_RESET = 12;
	
}
