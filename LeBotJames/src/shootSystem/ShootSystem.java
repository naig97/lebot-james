package shootSystem;

import def.*;
import motor.*;
import ios.IO;

import ch.ntb.inf.deep.runtime.ppc32.Task;

public class ShootSystem extends Task {
	
	private MotorSpeedController motorController;
	
	private Motor motorShoot;
	
	private IO ios;
	
	private float desiredSpeed;
	private float motorSpeed;
	
	private float desiredAngle;
	public boolean inAction;
	public boolean magnetAction;
	
	private int timeCounter;
	
	public ShootSystem () {
		
		motorShoot = new Motor(PinMap.MOTOR_SHOOT_PWM, PinMap.DIRECTION_MOTOR_SHOOT);
		
		motorController = new MotorSpeedController(Const.TS, PinMap.MOTOR_SHOOT_ENCODER_A, Const.USE_TPUA, Const.TICKS_PER_ROTATION,
				Const.MOTOR_VOLTAGE, Const.MOTOR_SHOOT_GEAR_TRANS_RATIO, Const.KP, Const.TN);
		
		ios = new IO();
		
		desiredSpeed = 0f;
		motorSpeed = 0f;
		inAction = false;
		magnetAction = false;
		timeCounter = 0;
		
		this.period = Const.SPEED_CONTROL_TASK_PERIOD;
		Task.install(this);
	}
	
	public void action () {
		
		if (timeCounter > Const.MAGNET_ON_TIME) {
			ios.electricMagnet.set(false);
			ios.liftingMagnet.set(false);
			magnetAction = false;
			timeCounter = 0;
			
		} else {
			timeCounter++;
		}
		
		if (inAction) {
			float actualAngle = motorController.getActualPosition();
			float angleDifference = desiredAngle - actualAngle;
			float desSpeed;
			
			if (Math.abs(angleDifference) < Const.ANGLE_ERROR) {
				desSpeed = 0;
				inAction = false;
			} else {
				desSpeed = angleDifference * Const.P_FAKTOR_SHOOTSYSTEM;
				if (desSpeed > Const.VELOCITY_SHOOTSYSTEM) {
					desSpeed = Const.VELOCITY_SHOOTSYSTEM;
				} else if (desSpeed < -Const.VELOCITY_SHOOTSYSTEM) {
					desSpeed = -Const.VELOCITY_SHOOTSYSTEM;
				}
			}
			drive(desSpeed);
		}
		
		if (motorSpeed < desiredSpeed) {
			motorSpeed += 0.01f;
			if (motorSpeed > desiredSpeed) {
				motorSpeed = desiredSpeed;
			}
			motorController.setDesiredSpeed(motorSpeed);
		} else if (motorSpeed > desiredSpeed) {
			motorSpeed -= 0.01f;
			if (motorSpeed < desiredSpeed) {
				motorSpeed = desiredSpeed;
			}
		}
		
		int speed = motorController.run();
		motorShoot.setSpeed(speed);
	}
	
	// desired angle in rad
	public void setPosition (float desiredAngle) {
		
		this.desiredAngle = desiredAngle;
		inAction = true;
	}
	
	// speed in mm/s
	public void drive (float speed) {
		speed = speed / (Const.GEAR_DIAMETER/2);
		desiredSpeed = speed;
		motorController.setDesiredSpeed(desiredSpeed);
	}
	
	public float getAngle () {
		return motorController.getActualPosition();
	}
	
	public void resetAngle () {
		motorController.absPos = 0;
	}
	
	public void driveFunnel () {
		timeCounter = 0;
		magnetAction = true;
		ios.liftingMagnet.set(true);
	}
	
	public void fire () {
		timeCounter = 0;
		magnetAction = true;
		ios.electricMagnet.set(true);
	}

}
