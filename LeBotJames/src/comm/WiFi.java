package comm;


import ch.ntb.inf.deep.runtime.mpc555.driver.MPIOSM_DIO;
import ch.ntb.inf.deep.runtime.mpc555.driver.RN131;
import ch.ntb.inf.deep.runtime.mpc555.driver.SCI;
import ch.ntb.inf.deep.runtime.util.CmdInt;
import def.*;

public class WiFi {
	
	public static RN131 wifi;
	private int receivedMessage;
	
	public WiFi() throws Exception {
		
		SCI sci2 = SCI.getInstance(SCI.pSCI2);
		sci2.start(115200, SCI.NO_PARITY, (short)8);
		wifi = new RN131(sci2.in, sci2.out, new MPIOSM_DIO(PinMap.WIFI_RESET, true));
	}
	
	public void sendData(int sendData) {
		if(connected()){
			wifi.cmd.writeCmd(sendData);
		}		
	}
	
	public int getData() {
		if(connected()) {
			CmdInt.Type type = wifi.cmd.readCmd();
			if(type == CmdInt.Type.Cmd){
				receivedMessage = wifi.cmd.getInt();
				return receivedMessage;
			}
			else {
				return receivedMessage;
			}
		}
		else {
			return 0;
		}
	}
	
	public boolean connected() {
//		if (wifi.connected()) {
//			ios.ledWifi.set(false);
//		}
//		else {
//			ios.ledWifi.set(true);
//		}
		return wifi.connected();
	}

}
