package motor;

import def.*;
import ch.ntb.inf.deep.runtime.mpc555.driver.TPU_PWM;
import ch.ntb.inf.deep.runtime.mpc555.driver.MPIOSM_DIO;
import motor.Encoder;

public class Motor {

	private MPIOSM_DIO dirPin;
	private TPU_PWM pwmMotor;
	private Encoder encoder;
	
//	public Motor (int pwmPin, int dirPin, int encoderPinA) {
//		
//		// PWM init
//		pwmMotor = new TPU_PWM(Const.USE_TPUA, pwmPin, Const.PWM_PERIOD, 0);
//		
//		// init Motor direction Pin
//		this.dirPin = new MPIOSM_DIO(dirPin, true);
//		
//		// initialize Encoder channels
//		encoder = new Encoder(encoderPinA);
//	}
	
	public Motor (int pwmPin, int dirPin) {
		// PWM init
		pwmMotor = new TPU_PWM(Const.USE_TPUA, pwmPin, Const.PWM_PERIOD, 0);

		// init Motor direction Pin
		this.dirPin = new MPIOSM_DIO(dirPin, true);
	}
	
	public void setSpeed (int speed) {
		if (speed >= 0) {
			dirPin.set(def.Const.vorwaerts);
		} else { 
			dirPin.set(def.Const.rueckwaerts);
			speed = (-1) * speed;
		}
		pwmMotor.update(speed);
	}
	
	public long getEncoderPosition () {
		return encoder.getPosition();
	}
	
	
	public void setEncoderPositionZero () {
		encoder.setZero();
	}
	
	public void stop() {
		this.setSpeed(0);
	}
}
