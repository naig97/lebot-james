package motor;

import ch.ntb.inf.deep.runtime.mpc555.Kernel;
import ch.ntb.inf.deep.runtime.mpc555.driver.TPU_FQD;
import def.Const;

/**
 * Speed controller (PI control) for DC motor. <br>
 * This controller uses two channels of the time processor unit 
 * and operates in sign-magnitude mode. <br>
 * 
 *  <strong>IMPORTANT:</strong> The motor and the encoder have to be connected carefully. A positive speed control must lead to a positive speed reading.
 *  If this is not the case you have to change either the connections to the motor or the signals of the encoder (but not both!). 
 */
public class MotorSpeedController {

	private TPU_FQD enc;
	
	private static final int fqd = 4;			// factor for fast quadrature decoding
	
	private float scale;						// scaling factor [rad/tick]
	private float b0, b1;						// controller coefficients
	private float umax;							// [V]
	
	private float desiredSpeed = 0,				// [1/s]
		controlValue = 0,						// [V]
		prevControlValue = 0;					// [V]
	
	private long time = 0, lastTime = 0;		// [us]
	private float dt;							// [s]
	private short actualPos, deltaPos, prevPos; // [ticks]
	public int absPos;
	private float speed = 0;					// [1/s]
	private float e = 0, e_1 = 0;				// [1/s]
	
	/**
	 * Create a new speed controller for a DC motor.
	 * @param ts task period in seconds [s]
	 * @param encChannelA TPU channel for the encoder signal A. For the signal B the channel of A + 1 will be used.
	 * @param useTPUA4Enc Time processing unit to use for FQD: true for TPU-A and false for TPU-B.
	 * @param encTPR impulse/ticks per rotation of the encoder.
	 * @param umax maximum output voltage of set value.
	 * @param i gear transmission ratio.
	 * @param kp controller gain factor. For experimental evaluating the controller parameters, begin with kp = 1.
	 * @param tn time constant of the controller. For experimental evaluating the controller parameters, set tn to the mechanical time constant of your axis. If the motor has a gear it's assumed that the torque of inertia of the rotor is dominant. That means you can set tn equals to the mechanical time constant of your motor. 
	 */
	public MotorSpeedController(float ts, int encChannelA, boolean useTPUA4Enc, int encTPR, float umax, float i, float kp, float tn) {
		// set parameters
		this.scale = (float)((2 * Math.PI) / (encTPR * fqd * i));
		this.b0 = kp * (1f + ts / (2f * tn));
		this.b1 = kp * (ts / (2f * tn) - 1f);
		this.umax = umax;
		
		// initialize FQD channels
		enc = new TPU_FQD(useTPUA4Enc, encChannelA);
	}

	
	/**
	 * Controller task method. Call this method periodically with the given period time (ts)!
	 */
	public int run() {
		// calculate exact time increment
		time = Kernel.time();
		dt = (time - lastTime) * 1e-6f;
		lastTime = time;
		
		// Read encoder and calculate actual speed
		actualPos = enc.getPosition();
		deltaPos = (short)(actualPos - prevPos);
		absPos += deltaPos;
		speed = (deltaPos * scale) / dt;
		
		// Calculate control value
		e = desiredSpeed - speed;
		controlValue = prevControlValue + b0 * e + b1 * e_1;
		
		//PARV: 11.1.2014
		//Anti Wind Up, so the duty cycle will always be  <= 100%
        if(controlValue > umax) {
               controlValue = umax;
        }
		
		// Save actual values for the next round
		prevPos = actualPos;
		e_1 = e;
		prevControlValue = controlValue;
		
		float dutyCycle = limitDutyCycle(controlValue / umax);
		return (int)(dutyCycle * Const.PWM_PERIOD);
	}
	
	/**
	 * Set desired speed.
	 * @param v desired speed in radian per second [1/s]
	 */
	public void setDesiredSpeed(float v) {
		this.desiredSpeed = v;
	}
	
	
	/** Returns the current speed.
	 * @return current speed in radian per second [1/s]
	 */
	public float getActualSpeed() {
		return speed;
	}
	
	/** Returns the current absolute position.
	 * @return absolute position in radian
	 */
	public float getActualPosition() {
		return absPos * scale;
	}
	
	private static float limitDutyCycle(float d) {
		if (d > 1)
			return 1;
		if (d < -1)
			return -1;
		return d;
	}
}

