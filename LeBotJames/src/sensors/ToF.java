package sensors;

import java.io.IOException;
import ch.ntb.inf.deep.runtime.mpc555.driver.SCI;
import ch.ntb.inf.deep.runtime.mpc555.driver.SCIInputStream;
import ch.ntb.inf.deep.runtime.ppc32.Task;

public class ToF extends Task {
	
	private SCIInputStream inputStream;
	private SCI sci1;
	private int[] latestValues = new int[9];
	private int loopIndex = 1; // FIXME: testing only, 0 for ball sensor
	private boolean eov = true;
	private int[] valueBuffer = new int[2];
	private int i = 0;
	
	public ToF() {
		
		sci1 = SCI.getInstance(SCI.pSCI1);
		sci1.start(9600, SCI.NO_PARITY, (short)8);
		
		inputStream = new SCIInputStream(sci1);
		
		this.period = 1;
		Task.install(this);
	}
	
	private void getDistanceForSensor(byte sensorId) {
		try {
			sci1.write(sensorId);
			eov = false;
			i = 0;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void readData(int loopIndex) {
		if (inputStream.available() > 0) {
			
			int b = inputStream.read();
			
			if (b == -2) {
				latestValues[loopIndex] = valueBuffer[0] * 127 + valueBuffer[1];
				eov = true;
			} else {
				valueBuffer[i] = b;
				i++;
			}
		}
	}
	
	public int getDistance(int sensorId) {
		return latestValues[sensorId];
	}
		
	public void action() {
		
		if (eov == true) {
			
			getDistanceForSensor((byte)loopIndex);
			
			if (loopIndex < 8) {
				loopIndex++;
			} else {
				loopIndex = 1; // FIXME: testing only, 0 for ball sensor
			}
		} else {
			readData(loopIndex);
		}
	}
}
