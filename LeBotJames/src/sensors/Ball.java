package sensors;

import ch.ntb.inf.deep.runtime.ppc32.Task;
import ch.ntb.inf.deep.runtime.mpc555.driver.QADC_AIN;

import def.*;


public class Ball extends Task {
		
	public short getBallSensor() {
		
		short ball = QADC_AIN.read(Const.USE_QADCA, PinMap.ADC_BALL_SENSOR);
		return ball;
	}
	
	public boolean getBallState() {
		 int value = QADC_AIN.read(Const.USE_QADCA, PinMap.ADC_BALL_SENSOR);
		 return (value < 100) ? true : false; // THRESHOLD
	}
	

}
