package sensors;

import ios.IO;
import def.*;

import ch.ntb.inf.deep.runtime.mpc555.driver.QADC_AIN;
import ch.ntb.inf.deep.runtime.ppc32.Task;

public class IR extends Task {
	
	private IO ios = new IO();
	private int sensState;
	private boolean measureGround = false;
	private int sensGFL = 1024;
	private int sensGFR = 1024;
	private int sensGBL = 1024;
	private int sensGBR = 1024;
	public boolean sensGFrontLeftBlack = false;
	public boolean sensGFrontRightBlack = false;
	public boolean sensGBackLeftBlack = false;
	public boolean sensGBackRightBlack = false;
	
	public IR () {
		sensState = Const.SENSOR_FRONT_L;
		ios.groundEn.set(!true);
		this.period = Const.IR_TASK_PERIOD;
		Task.install(this);
	}
	
	public void action() {
			
		if (!measureGround) {

			switch (sensState) {
			case Const.SENSOR_FRONT_L:
				ios.groundA.set(true);
				ios.groundB.set(false);
				break;

			case Const.SENSOR_FRONT_R:
				ios.groundA.set(false);
				ios.groundB.set(false);
				break;

			case Const.SENSOR_BACK_L:
				ios.groundA.set(true);
				ios.groundB.set(true);
				break;

			case Const.SENSOR_BACK_R:
				ios.groundA.set(false);
				ios.groundB.set(true);
				break;

			default:
				break;
			}
			measureGround = true;
		} else {
			int sensValActual = QADC_AIN.read(Const.USE_QADCA, PinMap.ADC_GROUND_SENSORS);
			int sensValDifference;
			switch (sensState) {
			case Const.SENSOR_FRONT_L:
				sensValDifference = sensGFL - sensValActual;
				sensGFL = sensValActual;
				if (sensValDifference < Const.SENSOR_VAL_DIF_W_B) { // from white to black
					sensGFrontLeftBlack = true; // on black line
				} else if (sensValDifference > Const.SENSOR_VAL_DIF_B_W) { // from black to white
					sensGFrontLeftBlack = false; // on white field
				}
				sensState = Const.SENSOR_FRONT_R;
				break;

			case Const.SENSOR_FRONT_R:
				sensValDifference = sensGFR - sensValActual;
				sensGFR = sensValActual;
				if (sensValDifference < Const.SENSOR_VAL_DIF_W_B) { // from white to black
					sensGFrontRightBlack = true; // on black line
				} else if (sensValDifference > Const.SENSOR_VAL_DIF_B_W) { // from black to white
					sensGFrontRightBlack = false; // on white field
				}
				sensState = Const.SENSOR_BACK_L;
				break;

			case Const.SENSOR_BACK_L:
				sensValDifference = sensGBL - sensValActual;
				sensGBL = sensValActual;
				if (sensValDifference < Const.SENSOR_VAL_DIF_W_B) { // from white to black
					sensGBackLeftBlack = true; // on black line
				} else if (sensValDifference > Const.SENSOR_VAL_DIF_B_W) { // from black to white
					sensGBackLeftBlack = false; // on white field
				}
				sensState = Const.SENSOR_BACK_R;
				break;

			case Const.SENSOR_BACK_R:
				sensValDifference = sensGBR - sensValActual;
				sensGBR = sensValActual;
				if (sensValDifference < Const.SENSOR_VAL_DIF_W_B) { // from white to black
					sensGBackRightBlack = true; // on black line
				} else if (sensValDifference > Const.SENSOR_VAL_DIF_B_W) { // from black to white
					sensGBackRightBlack = false; // on white field
				}
				sensState = Const.SENSOR_FRONT_L;
				break;
			}

			measureGround = false;
		}
	}
}
