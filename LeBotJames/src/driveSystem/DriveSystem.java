package driveSystem;

import def.*;

import ch.ntb.inf.deep.runtime.ppc32.Task;
import motor.Motor;
import motor.MotorSpeedController;


public class DriveSystem extends Task {
	
	private MotorSpeedController controllerLeft;
	private MotorSpeedController controllerRight;
	
	private Motor motorLeft;
	private Motor motorRight;
	
	private float desiredSpeedLeft;
	private float desiredSpeedRight;
	
	private float motorSpeedLeft;
	private float motorSpeedRight;
	
	public boolean turn = false;

	private float turnAngle;
	public boolean line = false;
	
	public boolean dist = false;
	private float distance;
	public float left;
	public float right;
	
	public DriveSystem () {
			
		motorLeft = new Motor(PinMap.MOTOR_LEFT_PWM, PinMap.DIRECTION_MOTOR_LEFT);
		motorRight = new Motor(PinMap.MOTOR_RIGHT_PWM, PinMap.DIRECTION_MOTOR_RIGHT);

		controllerLeft = new MotorSpeedController(Const.TS, PinMap.MOTOR_LEFT_ENCODER_A, Const.USE_TPUA, Const.TICKS_PER_ROTATION,
				Const.MOTOR_VOLTAGE, Const.MOTOR_DRIVE_GEAR_TRANS_RATIO, Const.KP, Const.TN);
		
		controllerRight = new MotorSpeedController(Const.TS, PinMap.MOTOR_RIGHT_ENCODER_A, Const.USE_TPUA, Const.TICKS_PER_ROTATION,
				Const.MOTOR_VOLTAGE, Const.MOTOR_DRIVE_GEAR_TRANS_RATIO, Const.KP, Const.TN);
		
		desiredSpeedLeft = 0f;
		desiredSpeedRight = 0f;
		motorSpeedLeft = 0f;
		motorSpeedRight = 0f;
		
		this.period = Const.SPEED_CONTROL_TASK_PERIOD;
		Task.install(this);
	}
	
	public void action() {
		
		if (turn) {
			float actualAngle = controllerLeft.getActualPosition();
			float angleDifference = turnAngle - actualAngle;
			float desSpeed;
			
			if (Math.abs(angleDifference) < Const.ANGLE_ERROR_TURN) {
				desSpeed = 0;
				turn = false;
				
			} else {
				desSpeed = angleDifference * Const.P_FAKTOR_TURN;
				if (desSpeed > Const.TURN_VELOCITY) {
					desSpeed = Const.TURN_VELOCITY;
				} else if (desSpeed < -Const.TURN_VELOCITY) {
					desSpeed = -Const.TURN_VELOCITY;
				}
			}
			driveSingle(desSpeed, -desSpeed);
		}
		
		if (dist) {
			left = controllerLeft.getActualPosition()*-1;
			right = controllerRight.getActualPosition();
			
			if (distance <= left || distance <= right) {
				controllerLeft.setDesiredSpeed(0);
				controllerRight.setDesiredSpeed(0);
				dist = false;
				}
		}
		
		if (motorSpeedLeft < desiredSpeedLeft) {
			motorSpeedLeft += 0.01f;
			if (motorSpeedLeft > desiredSpeedLeft) {
				motorSpeedLeft = desiredSpeedLeft;
			}
			controllerLeft.setDesiredSpeed(motorSpeedLeft);
		} else if (motorSpeedLeft > desiredSpeedLeft) {
			motorSpeedLeft -= 0.01f;
			if (motorSpeedLeft < desiredSpeedLeft) {
				motorSpeedLeft = desiredSpeedLeft;
			}
			controllerLeft.setDesiredSpeed(motorSpeedLeft);
		}

		if (motorSpeedRight < desiredSpeedRight) {
			motorSpeedRight += 0.01f;
			if (motorSpeedRight > desiredSpeedRight) {
				motorSpeedRight = desiredSpeedRight;
			}
			controllerRight.setDesiredSpeed(motorSpeedRight);
		} else if (motorSpeedRight > desiredSpeedRight) {
			motorSpeedRight -= 0.01f;
			if (motorSpeedRight < desiredSpeedRight) {
				motorSpeedRight = desiredSpeedRight;
			}
			controllerRight.setDesiredSpeed(motorSpeedRight);
		}
		
		
		
		int speedLeft = controllerLeft.run();
		int speedRight = controllerRight.run();
		motorLeft.setSpeed(speedLeft);
		motorRight.setSpeed(speedRight);
	}
	
	// speed in mm/s
	public void drive(float speed) {
		speed = speed / (Const.WHEEL_DIAMETER / 2);
		desiredSpeedLeft = -speed;
		desiredSpeedRight = speed;
	}
	
	public void driveDist(float speed, float distance) {
		this.distance = distance / (Const.WHEEL_DIAMETER / 2);
		speed = speed / (Const.WHEEL_DIAMETER / 2);
		resetAngle();
//		drive(speed);
		controllerLeft.setDesiredSpeed(-speed);
		controllerRight.setDesiredSpeed(speed);
		dist = true;
	}
	
	public int links() {
		return (int) left;
	}
	
	// speed in mm/s
	public void driveSingle(float speedRight, float speedLeft) {
		speedRight = speedRight / (Const.WHEEL_DIAMETER / 2);
		speedLeft = speedLeft / (Const.WHEEL_DIAMETER / 2);
		desiredSpeedLeft = -speedLeft;
		desiredSpeedRight = speedRight;
	}
	
	// positive for turn counter clockwise, negative for clockwise
	public void turn90(float anglefaktor) {
		resetAngle();
		turnAngle = Const.DEGREE_FOR_90DEG * anglefaktor; // 4.538rad for 90
		turn = true;
	}
	
	// positive for turn counter clockwise, negative for clockwise
		public void turn180(float anglefaktor) {
			resetAngle();
			turnAngle = Const.DEGREE_FOR_90DEG * 2 * anglefaktor; 
			turn = true;
		}
	
	public void resetAngle() {
		controllerLeft.absPos = 0;
		controllerRight.absPos = 0;
	}
}
