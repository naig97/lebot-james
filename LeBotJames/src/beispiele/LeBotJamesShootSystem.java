package beispiele;

import java.io.PrintStream;

import ch.ntb.inf.deep.runtime.mpc555.driver.QADC_AIN;
import ch.ntb.inf.deep.runtime.mpc555.driver.SCI;
import ch.ntb.inf.deep.runtime.ppc32.Task;
import def.Const;
import def.PinMap;
import ios.IO;
import shootSystem.ShootSystem;


public class LeBotJamesShootSystem extends Task {
	
	public IO ios = new IO();
	private ShootSystem shootSystem;
	private boolean stateS1;
	private boolean stateS2;
	private boolean stateMan;

	public LeBotJamesShootSystem () {
		ios.pinInitState();
		shootSystem = new ShootSystem();
	}
	
	public void action() {
		
		int speed = QADC_AIN.read(Const.USE_QADCA, PinMap.ADC_VELOCITY);
		speed = (speed-1) / 205;
		if (!ios.manualDir.get()) {
			speed = -speed;
		}
		shootSystem.drive((float)speed);
		
		boolean stateS1Actual = ios.switch1.get();
		boolean stateS2Actual = ios.switch2.get();
		boolean stateSMan = ios.manualSet.get();
		
		if (stateS1 != stateS1Actual) {
			if (stateS1Actual == true) { // positive Flank
				System.out.println("pos");
				shootSystem.fire();
				shootSystem.setPosition(Const.POSITION_ZERO);
			} else { // negative Flank
				
			}
			System.out.print("S1: ");
			System.out.println(stateS1Actual);
			stateS1 = stateS1Actual;
		}
		
		if (stateS2 != stateS2Actual) {
			if (stateS2Actual == true) { // positive Flank
				shootSystem.setPosition(Const.POSITION_SHOOT_BASKET);
			} else { // negative Flank
				
			}
			System.out.print("S2: ");
			System.out.println(stateS2Actual);
			stateS2 = stateS2Actual;
		}
		
		if (stateMan != stateSMan) {
			if (stateSMan == true) {
				System.out.println(shootSystem.getAngle());
			}
			stateMan = stateSMan;
		}
	}
	
	static {
		
		//1) Initialisiere SCI1 (9600 8N1) 
		SCI sci1 = SCI.getInstance(SCI.pSCI1);
				
		sci1.start(9600, SCI.NO_PARITY, (short) 8);
				
		//2) Benutze SCI1 f�r Standard-Output
		System.out = new PrintStream(sci1.out);
		
//		System.out.println("Ticks:");
				
		LeBotJamesShootSystem t = new LeBotJamesShootSystem();
		t.period = 10;
		Task.install(t);
	}
}
