package beispiele;

import ch.ntb.inf.deep.runtime.ppc32.Task;
import comm.WiFi;
import def.Const;
import def.PinMap;
import driveSystem.DriveSystem;
import ios.IO;

import motor.Motor;

import ch.ntb.inf.deep.runtime.mpc555.driver.SCI;
import java.io.PrintStream;
import ch.ntb.inf.deep.runtime.mpc555.driver.QADC_AIN;

import sensors.IR;

public class LeBotJamesDrive extends Task {
	
	private IR sens;
	private DriveSystem driveSystem;
	private WiFi wifi; 
	
	private Motor motor;
	
	private int timeCounter = 0;
	
	private int akkuBadValue;
	private boolean akkuOk;
	private boolean manuelMode;
	
	private boolean stateS1;
	private boolean stateS2;
	private boolean stateSModus;
	private boolean stateSDir;
	
	private boolean sensGFrontLeftBlack = false;
	private boolean sensGFrontRightBlack = false;
	private boolean sensGBackLeftBlack = false;
	private boolean sensGBackRightBlack = false;
	
	private States state;
	private int vcounter = 0;
	private int vwlan = 0;
	
	public IO ios = new IO();

	public LeBotJamesDrive() throws Exception {
		
		driveSystem = new DriveSystem();
		sens = new IR();
		
		driveSystem.drive(0);
		wifi = new WiFi();
		
		motor = new Motor(PinMap.MOTOR_SHOOT_PWM, PinMap.DIRECTION_MOTOR_SHOOT);
		
		stateS1 = ios.switch1.get();
		stateS2 = ios.switch2.get();
		stateSModus = ios.manualSet.get();
		stateSDir = ios.manualDir.get();
		
		akkuOk = true;
		manuelMode = false;
		akkuBadValue = Const.AKKU_BAD_VALUE;
		
		ios.pinInitState();
		
		motor.setSpeed(0);
		
		System.out.println("Finish init");
		state = States.start;
	}
	
	private static enum States {
		start,
		manuelMODE,
		driveX,
		driveY
	}
	
	public void action() {
		
		switch (state) {
		case start:
			if (manuelMode) {
				state = States.manuelMODE;
			}
			//state = States.driveX;
			
			break;
			
		case manuelMODE:
			if (manuelMode) {
				ios.ledManualOn.set(true);
				int speed = QADC_AIN.read(Const.USE_QADCA, PinMap.ADC_VELOCITY);
				speed = speed / 17;
				if (!stateSDir) {
					speed = -speed;
				}
//				motor.setSpeed(speed);
				driveSystem.driveSingle(speed, -speed);
				
			} else {
				
				ios.ledManualOn.set(false);
				motor.setSpeed(0);
				System.out.println("Case Start");
				state = States.start;
			}
			break;

		case driveX:
			//if (!turn) {
				if (sens.sensGFrontRightBlack && sens.sensGFrontLeftBlack) {
					driveSystem.drive(vcounter);
				} else if (sens.sensGFrontRightBlack) {
					driveSystem.driveSingle(vcounter - 5, vcounter);
				} else if (sens.sensGFrontLeftBlack) {
					driveSystem.driveSingle(vcounter, vcounter - 5);
				} else {
					driveSystem.drive(vcounter);
				}
			break;

		default:
			break;
		}
		
		if (timeCounter >= 10) {
			short akku = QADC_AIN.read(Const.USE_QADCA, PinMap.ADC_BATTERY);
			if (akku < akkuBadValue) { // Akku check
				if (akkuOk) {
					akkuBadValue += 3; // Hysteresis for voltage ripples 
				}
				akkuOk = false;
				ios.ledBatteryBad.set(true);
			} else {
				akkuOk = true;
				akkuBadValue = Const.AKKU_BAD_VALUE;
				ios.ledBatteryBad.set(false);
			}
			
			boolean stateS1Actual = ios.switch1.get();
			boolean stateS2Actual = ios.switch2.get();
			boolean stateSModusActual = ios.manualSet.get();
			boolean stateSDirActual = ios.manualDir.get();
			
			if (stateS1 != stateS1Actual) {
				if (stateS1Actual == true) { // positive Flank
					System.out.println("pos");
					driveSystem.turn90(-1);
				} else { // negative Flank
					driveSystem.turn90(1);
				}
				System.out.print("S1: ");
				System.out.println(stateS1Actual);
				stateS1 = stateS1Actual;
			}
			
			if (stateS2 != stateS2Actual) {
				if (stateS2Actual == true) { // positive Flank
					driveSystem.drive(100f);
				} else { // negative Flank
					driveSystem.drive(0f);
				}
				System.out.print("S2: ");
				System.out.println(stateS2Actual);
				stateS2 = stateS2Actual;
			}
			
			if (stateSModus != stateSModusActual) {
				if (stateSModusActual == true) { // positive Flank
					manuelMode = true;
				} else { // negative Flank
					manuelMode = false;
				}
				System.out.print("SMod: ");
				System.out.println(stateSModusActual);
				stateSModus = stateSModusActual;
			}
			
			if (stateSDir != stateSDirActual) {
				if (stateSDirActual == true) { // positive Flank
					
				} else { // negative Flank
					
				}
				System.out.print("SDir: ");
				System.out.println(stateSDirActual);
				stateSDir = stateSDirActual;
			}
			
			if (sensGFrontLeftBlack != sens.sensGFrontLeftBlack) {
				if (sens.sensGFrontLeftBlack == true) {
					System.out.println("SensVL: auf Schwarzer linie");
				} else {
					System.out.println("SensVL: auf weissem Boden");
				}
				sensGFrontLeftBlack = sens.sensGFrontLeftBlack;
			}
			
			if (sensGFrontRightBlack != sens.sensGFrontRightBlack) {
				if (sens.sensGFrontRightBlack == true) {
					System.out.println("SensVR: auf Schwarzer linie");
				} else {
					System.out.println("SensVR: auf weissem Boden");
				}
				sensGFrontRightBlack = sens.sensGFrontRightBlack;
			}
			
			if (sensGBackLeftBlack != sens.sensGBackLeftBlack) {
				if (sens.sensGBackLeftBlack == true) {
					System.out.println("SensBL: auf Schwarzer linie");
				} else {
					System.out.println("SensBL: auf weissem Boden");
				}
				sensGBackLeftBlack = sens.sensGBackLeftBlack;
			}
			
			if (sensGBackRightBlack != sens.sensGBackRightBlack) {
				if (sens.sensGBackRightBlack == true) {
					System.out.println("SensBR: auf Schwarzer linie");
				} else {
					System.out.println("SensBR: auf weissem Boden");
				}
				sensGBackRightBlack = sens.sensGBackRightBlack;
			}
			vwlan = wifi.getData();
			if (vwlan > 60) {
				vwlan = 60 - vwlan;
			}
			if (vcounter < vwlan)
				vcounter += 5;
			else if (vcounter > vwlan)
				vcounter -= 5;
			timeCounter = 0;
		} else {
			timeCounter++;
		}
	}
	
	static {
		//1) Initialisiere SCI1 (9600 8N1) 
		SCI sci1 = SCI.getInstance(SCI.pSCI1);
		
		sci1.start(9600, SCI.NO_PARITY, (short) 8);
		
		//2) Benutze SCI1 f�r Standard-Output
		System.out = new PrintStream(sci1.out);
		
		try {

			LeBotJamesDrive t = new LeBotJamesDrive();
			t.period = Const.MAIN_TASK_PERIOD;
			Task.install(t);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
