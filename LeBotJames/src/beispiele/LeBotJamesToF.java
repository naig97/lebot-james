package beispiele;

import java.io.IOException;
import java.io.PrintStream;

//import ch.ntb.inf.deep.runtime.mpc555.driver.SCI;
import ch.ntb.inf.deep.runtime.ppc32.Task;
import comm.WiFi;
import def.*;
import ios.IO;


import sensors.ToF;
import sensors.Ball;

public class LeBotJamesToF extends Task {

	private ToF tof;
	public WiFi wifi;
	private Ball ballSens;
	
	public IO ios = new IO();
	
	private int i = 1;
	
	public LeBotJamesToF() throws Exception {
		
		ios.pinInitState();
		
		wifi = new WiFi();
		tof = new ToF();
		ballSens = new Ball();
	}
	
	public void action() {
		
//		wifi.sendData(ballSens.getBallSensor());
		
		wifi.sendData(i);
		wifi.sendData(tof.getDistance(i));

		
		if (i < 8) {
			i++;
		} else {
			i = 1;
		}
	}
	
	static {
		LeBotJamesToF t;
		try {
			t = new LeBotJamesToF();
			t.period = 2000;
			Task.install(t);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
