package beispiele;

import ch.ntb.inf.deep.runtime.ppc32.Task;
import ch.ntb.inf.deep.runtime.mpc555.driver.QADC_AIN;
import def.*;
import driveSystem.DriveSystem;
import ios.IO;
import motor.Motor;

import ch.ntb.inf.deep.runtime.mpc555.driver.SCI;
import java.io.PrintStream;

import comm.WiFi;

public class LeBotJamesFabio extends Task {

	public IO ios = new IO();
//	private Drive_System driveSystem;
//	private WiFi wifi;
	private Motor motorl = new Motor(PinMap.MOTOR_LEFT_PWM, PinMap.DIRECTION_MOTOR_LEFT/*, PinMap.MOTOR_LEFT_ENCODER_A*/);
	private Motor motorr = new Motor(PinMap.MOTOR_RIGHT_PWM, PinMap.DIRECTION_MOTOR_RIGHT/*, PinMap.MOTOR_RIGHT_ENCODER_A*/);
	private Motor motora = new Motor(PinMap.MOTOR_SHOOT_PWM, PinMap.DIRECTION_MOTOR_SHOOT/*, PinMap.MOTOR_SHOOT_ENCODER_A*/);
	

	private float i = 0;

	public LeBotJamesFabio() {
		
		motora.setSpeed(0);
		

//		driveSystem = new Drive_System();
//		try {
//			wifi = new WiFi();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		ios.pinInitState();
//		driveSystem.drive(0);
	}

	public void action() {
		int speed = QADC_AIN.read(Const.USE_QADCA, PinMap.ADC_VELOCITY);
		speed = speed / 17;
		if (ios.manualDir.get()) {
			speed = -speed;
		}
		System.out.println(speed);
		motora.setSpeed(speed);
		
		System.out.println(motora.getEncoderPosition());
//		float speed = (float) wifi.getData();
//		if (speed > 200) {
//			speed = 200 - speed;
//		}
//		
//		driveSystem.drive(speed);
	}

	static {
		// 1) Initialisiere SCI1 (9600 8N1)
		SCI sci1 = SCI.getInstance(SCI.pSCI1);

		sci1.start(9600, SCI.NO_PARITY, (short) 8);

		// 2) Benutze SCI1 f�r Standard-Output
		System.out = new PrintStream(sci1.out);

		LeBotJamesFabio t = new LeBotJamesFabio();
		t.period = 100;
		Task.install(t);
	}
}
