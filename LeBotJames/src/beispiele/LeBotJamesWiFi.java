package beispiele;

import java.io.PrintStream;

import ch.ntb.inf.deep.runtime.mpc555.driver.SCI;
import ch.ntb.inf.deep.runtime.ppc32.Task;
import def.Const;
import ios.IO;
import comm.WiFi;


// am schluss m�mer denn aifach alli LBJ klassana zemabastla,
// sind etz aifach trennt zumd modul einzeln testa

public class LeBotJamesWiFi extends Task {
	
	
	public IO ios = new IO();
	public WiFi wifi; 

	public LeBotJamesWiFi() throws Exception {
		
		ios.pinInitState();
		wifi = new WiFi();
	}
	
	public void action() {
	
		System.out.println(wifi.getData());
		wifi.sendData(wifi.getData()+1);
		
	}
	
	static {

		//Init SCI1 (19200 8N1)
		SCI sci1 = SCI.getInstance(SCI.pSCI1);
		sci1.start(9600, SCI.NO_PARITY, (short)8);
		//Hook SCI1.out on System.out
		System.out = new PrintStream(sci1.out);	
		
		// Install and start the task
		try{
			LeBotJamesWiFi t = new LeBotJamesWiFi();
			t.period = 500;
			Task.install(t);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
