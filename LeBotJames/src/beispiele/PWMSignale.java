package beispiele;

import ch.ntb.inf.deep.runtime.mpc555.driver.MPIOSM_DIO;
import ch.ntb.inf.deep.runtime.mpc555.driver.TPU_DIO;
import ch.ntb.inf.deep.runtime.mpc555.driver.TPU_PWM;
import ch.ntb.inf.deep.runtime.mpc555.driver.SCI;
import java.io.PrintStream;
import ch.ntb.inf.deep.runtime.ppc32.Task;

	public class PWMSignale  extends Task { 
		// input encoder motors
//		public TPU_DIO mlEncA = new TPU_DIO(true, 0, false);
//		public TPU_DIO mlEncB = new TPU_DIO(true, 1, false);
//		public TPU_DIO mrEncA = new TPU_DIO(true, 2, false);
//		public TPU_DIO mrEncB = new TPU_DIO(true, 3, false);
//		public TPU_DIO maEncA = new TPU_DIO(true, 4, false);
//		public TPU_DIO maEncB = new TPU_DIO(true, 5, false);
		
		// output PWM motors
//		public TPU_DIO mlPWM = new TPU_DIO(true, 6, true);
//		public TPU_DIO mrPWM = new TPU_DIO(true, 7, true);
//		public TPU_DIO maPWM = new TPU_DIO(true, 8, true);
		
		// output deb�g LEDs
		public TPU_DIO led1 = new TPU_DIO(true, 9, true);
		public TPU_DIO led2 = new TPU_DIO(true, 10, true);
		public TPU_DIO led3 = new TPU_DIO(true, 11, true);
//		
		// input deb�g switches
//		public TPU_DIO s1 = new TPU_DIO(true, 12, false);
//		public TPU_DIO s2 = new TPU_DIO(true, 13, false);
//		
		// anal input
//		public MPIOSM_DIO adcAkku = new MPIOSM_DIO(20, false);
//		public MPIOSM_DIO adcBoden = new MPIOSM_DIO(21, false);
//		public MPIOSM_DIO adcBall = new MPIOSM_DIO(22, false);
//		public MPIOSM_DIO adcPot = new MPIOSM_DIO(23, false);
		
		// output LEDs
		public MPIOSM_DIO ledWifi = new MPIOSM_DIO(13, true);
		public MPIOSM_DIO ledAkkuBad = new MPIOSM_DIO(14, true);
		public MPIOSM_DIO ledAkkuOk = new MPIOSM_DIO(12, true);
		public MPIOSM_DIO manuellOn = new MPIOSM_DIO(11, true);
//		
		// output ground sensors 
//		public MPIOSM_DIO groundEn = new MPIOSM_DIO(10, true);
//		public MPIOSM_DIO groundB = new MPIOSM_DIO(9, true);
//		public MPIOSM_DIO groundA = new MPIOSM_DIO(8, true);
//		
		// input manual modus switches 
//		public MPIOSM_DIO manualDir = new MPIOSM_DIO(7, false);
//		public MPIOSM_DIO manualSet = new MPIOSM_DIO(6, false);
//		
		// output motor driver
//		public MPIOSM_DIO maDir = new MPIOSM_DIO(5, true);
//		public MPIOSM_DIO mrDir = new MPIOSM_DIO(0, true);
		public MPIOSM_DIO mlDir = new MPIOSM_DIO(1, true);
//		
		// output stuffy McStuffface
//		public MPIOSM_DIO liftingMagnet = new MPIOSM_DIO(2, true);
//		public MPIOSM_DIO charge = new MPIOSM_DIO(3, true);
//		public MPIOSM_DIO magnet = new MPIOSM_DIO(4, true);
//		
		// serial comm
//		public MPIOSM_DIO mTX = new MPIOSM_DIO(247, true);
//		public MPIOSM_DIO wTX = new MPIOSM_DIO(246, true);
//		public MPIOSM_DIO mRX = new MPIOSM_DIO(245, false);
//		public MPIOSM_DIO wRX = new MPIOSM_DIO(244, false);
		
		private motor.Motor motor;
		private final int testChannel  = 6;
		byte tpuPin = 0;
		
		private final boolean useTPUA = true;
		
		// pwmPeriod in TimeBase Unit (50�000 ns)
		private final int pwmPeriod  = 50000 / TPU_PWM.tpuTimeBase; // maximale Geschwindigkeit 62
		
		
			public int highTime = -1;
			
			private TPU_PWM pwm;
			public PWMSignale() {
				
				//motor = new motor.Motor (def.PinMap.MOTOR_LEFT_PWM, def.PinMap.DIRECTION_MOTOR_LEFT, def.PinMap.MOTOR_LEFT_ENCODER_A);
				motor.setSpeed(62);
				
				led1.set(false);
				led2.set(false);
				led3.set(false);
				
				ledWifi.set(false);
				ledAkkuBad.set(false);
				ledAkkuOk.set(false);
				manuellOn.set(false);
//				
//				pwm = new TPU_PWM(useTPUA, testChannel, pwmPeriod, highTime);
				period = 10; // Periodenlaenge des Tasks in ms
				Task.install(this);
			} 
			public void action() {
				System.out.print(" > ");
				System.out.println(motor.getEncoderPosition());
			}
			
			static 
			{ // Task Initialisierung 
				new PWMSignale();
				
				//1) Initialisiere SCI1 (9600 8N1) 
				SCI sci1 = SCI.getInstance(SCI.pSCI1);
				
				sci1.start(9600, SCI.NO_PARITY, (short) 8);
				
				//2) Benutze SCI1 f�r Standard-Output
				System.out = new PrintStream(sci1.out);
			} 
}