package beispiele;

import java.io.PrintStream;

import ch.ntb.inf.deep.runtime.mpc555.driver.SCI;
import ch.ntb.inf.deep.runtime.ppc32.Task;
import def.Const;
import ios.IO;
import sensors.IR;


// am schluss m�mer denn aifach alli LBJ klassana zemabastla,
// sind etz aifach trennt zumd modul einzeln testa

public class LeBotJamesIR extends Task {

	public IO ios = new IO();
	public IR ir; 

	public LeBotJamesIR() throws Exception {
		
		ios.pinInitState();
	}
	
	public void action() {
		
//		System.out.println("dini mama");
//		System.out.println(ir.getBallSensor());
		
	}
	
	static {

		// Init SCI1 (19200 8N1)
		SCI sci1 = SCI.getInstance(SCI.pSCI1);
		sci1.start(9600, SCI.NO_PARITY, (short)8);
		//Hook SCI1.out on System.out
		System.out = new PrintStream(sci1.out);	
		
		// Install and start the task
		try{
			LeBotJamesIR t = new LeBotJamesIR();
			t.period = 1000;
			Task.install(t);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
