package main;

import ch.ntb.inf.deep.runtime.ppc32.Task;
import ch.ntb.inf.deep.runtime.mpc555.driver.QADC_AIN;
import ch.ntb.inf.deep.runtime.mpc555.driver.SCI;
import java.io.PrintStream;

import def.Const;
import def.PinMap;
import comm.WiFi;
import shootSystem.ShootSystem;
import driveSystem.DriveSystem;
import ios.IO;
import sensors.IR;
import sensors.ToF;
import sensors.Ball;

public class LeBotJames_newer_old extends Task {

	private DriveSystem driveSystem;
	private ShootSystem shootSystem;
	private WiFi wifi;
	private IR irSens;
	private ToF tofSens;
	private Ball ballSens;
	private IO ios;

	private boolean stateSWMode;
	private mainStates mainState;
	private conStates conState;
	private withStates withState;
	private withoutStates withoutState;

	private int akkuBadValue;
	private boolean akkuOk;
	private boolean manuelMode;

	private int oldDist;

	public LeBotJames_newer_old() throws Exception {

		driveSystem = new DriveSystem();
		shootSystem = new ShootSystem();
		wifi = new WiFi();
		irSens = new IR();
		tofSens = new ToF();
		ballSens = new Ball();
		ios = new IO();

		ios.pinInitState();
		driveSystem.drive(0);
		shootSystem.resetAngle();
		shootSystem.setPosition(Const.POSITION_GET_BALL);

		akkuOk = true;
		manuelMode = false;
		oldDist = 3000;

		mainState = mainStates.CONNECTION;
		conState = conStates.connect;
	}

	private static enum mainStates {
		CONNECTION, WITHBALL, WITHOUTBALL
	}

	private static enum conStates {
		connect, connectACK, ballPos1
	}

	private static enum withStates {
		stayPos1, wait1, shot1, getBall1, ballIn1, turn1, shot2, turn2, driveX1, turn3, driveY1, driveY1_2, turn4,
		driveY1_3, driveY1_4, driveY1_5, turn5, turn7, driveX2, turn6, wait2, goToPos7, readyPos7, wait7, ballIn7,
		shot8, turnIntoX2, drive_X2
	}
	
	private static enum withoutStates {
		goToPos2, shot3, driveX1, drive_X1, driveX2, goToPos6, turn1, driveY1, stopPos2, wait3, turn6, wait6, ballIn6,
		ready6, shot9, turn8, driveYFinal, driveXR, driveXL, turnOn2
	}
	//FIXME:	nur dasi nid immer ufascrolla muas 
	
	private void driveLine() {
		if (irSens.sensGFrontRightBlack && irSens.sensGFrontLeftBlack) {
			driveSystem.drive(Const.MAX_DRIVE_SPEED);
		} else if (irSens.sensGFrontRightBlack) {
			driveSystem.driveSingle(Const.MAX_DRIVE_SPEED - 10, Const.MAX_DRIVE_SPEED);
		} else if (irSens.sensGFrontLeftBlack) {
			driveSystem.driveSingle(Const.MAX_DRIVE_SPEED, Const.MAX_DRIVE_SPEED - 10);
		} else {
			driveSystem.drive(Const.MAX_DRIVE_SPEED);
		}
	}
	
	private boolean pos90Deg (int sens1, int sens2) {
		int difference = tofSens.getDistance(sens1) - tofSens.getDistance(sens2);
		wifi.sendData(difference);
		if (Math.abs(difference) >= 1) {
			if (difference < 0) {
				driveSystem.driveSingle(-3, 3);
			} else {
				driveSystem.driveSingle(3, -3);
			}
			return false;
		} else {
			return true;
		}
	}

	public void action() {
		short akku = QADC_AIN.read(Const.USE_QADCA, PinMap.ADC_BATTERY);
		if (akku < akkuBadValue) { // Akku check
			if (akkuOk) {
				akkuBadValue += 3; // Hysteresis for voltage ripples
			}
			akkuOk = false;
			ios.ledBatteryBad.set(true);
		} else {
			akkuOk = true;
			akkuBadValue = Const.AKKU_BAD_VALUE;
			ios.ledBatteryBad.set(false);
		}

		switch (mainState) {

		// Connectivity check

		case CONNECTION:
			switch (conState) {
			case connect:
				if (wifi.getData() == Const.START) {
					wifi.sendData(Const.CONNECTION_CHECK);
					conState = conStates.connectACK;
				}
				break;

			case connectACK:
				if (wifi.getData() == Const.CONNECTION_OK) {
					wifi.sendData(Const.CONNECTION_OK);
					ios.ledWifi.set(false);
					shootSystem.driveFunnel();
					conState = conStates.ballPos1;
				}
				break;

			case ballPos1:
				if (!shootSystem.inAction) {
					if (ballSens.getBallState()) {
						wifi.sendData(Const.BALL_TRUE);
						shootSystem.setPosition(Const.POSITION_SHOOT_HORIZONTAL);
						mainState = mainStates.WITHBALL;
						withState = withStates.stayPos1;

					} else {
						if (wifi.getData() == Const.BALL_TRUE) {
							wifi.sendData(Const.BALL_FALSE);
							mainState = mainStates.WITHOUTBALL;
							withoutState = withoutStates.goToPos2;
							wifi.sendData(Const.GOTO_POS2);
						}
//						mainState = mainStates.WITHBALL;
//						withState = withStates./*stayPos1*/turn1;
					}
				}
				break;
			}
			break;

		// start with ball
		// ------------------------------------------------------------------------------------------------

		case WITHBALL:
			switch (withState) {

			case stayPos1:
				if (!shootSystem.inAction) {
					withState = withStates.shot1;
				}
				break;

			case shot1:
				if (wifi.getData() == Const.READY_POS2) {
					wifi.sendData(Const.SHOOT_POS2);
					shootSystem.fire();
					shootSystem.setPosition(Const.POSITION_ZERO);
					withState = withStates.wait1;
				}
				break;

			case wait1:
				if (!shootSystem.inAction) {
					shootSystem.setPosition(Const.POSITION_GET_BALL);
					withState = withStates.getBall1;
				}
				break;

			case getBall1:
				if (!shootSystem.inAction) {
					wifi.sendData(Const.READY_POS1);
					withState = withStates.ballIn1;
				}
				break;

			case ballIn1:
				if (ballSens.getBallState() && (wifi.getData() == Const.SHOOT_POS1)) {
					wifi.sendData(Const.BALL_TRUE);
					withState = withStates.turn1;
				}
				break;

			case turn1:
				driveSystem.turn90(-1);
				shootSystem.setPosition(Const.POSITION_SHOOT_VERTICAL);
				withState = withStates.shot2;
				break;

			case shot2:
				if (!shootSystem.inAction && !driveSystem.turn) {
					if (wifi.getData() == Const.READY_POS4) {
						wifi.sendData(Const.SHOOT_POS4);
						shootSystem.fire();
						shootSystem.setPosition(Const.POSITION_ZERO);
						wifi.sendData(Const.GOTO_POS7);
						driveSystem.turn90(1);
						withState = withStates.turn2;
					}
				}
				break;

			case turn2:
				if (!driveSystem.turn) {
					driveSystem.drive(Const.MAX_DRIVE_SPEED);
					withState = withStates.driveX1;
				}
				break;

			case driveX1:
				
				if (driveSystem.turn) {
					break;
				}
				
				driveLine();
				
				if (tofSens.getDistance(Const.SENSOR_BACK_L) > 265 && tofSens.getDistance(Const.SENSOR_BACK_L) < 290
						|| tofSens.getDistance(Const.SENSOR_BACK_R) > 265
								&& tofSens.getDistance(Const.SENSOR_BACK_R) < 290) {
					if (tofSens.getDistance(Const.SENSOR_RIGHT_B) > 60) {
						wifi.sendData(tofSens.getDistance(Const.SENSOR_RIGHT_B));
						wifi.sendData(1);
						driveSystem.drive(0);
						driveSystem.turn90(-1.2f);
						withState = withStates.turn3;
					}
				} else if (tofSens.getDistance(Const.SENSOR_FRONT_L) > 280
						&& tofSens.getDistance(Const.SENSOR_FRONT_L) < 285
						|| tofSens.getDistance(Const.SENSOR_FRONT_R) > 280
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) < 285) {

					if (tofSens.getDistance(Const.SENSOR_RIGHT_F) > 60) {
						wifi.sendData(2);
						driveSystem.turn90(-1.2f);
						withState = withStates.turn3;
					}

				} else if (tofSens.getDistance(Const.SENSOR_FRONT_L) > 100
						&& tofSens.getDistance(Const.SENSOR_FRONT_L) < 120
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) > 100
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) < 120) {

					if (tofSens.getDistance(Const.SENSOR_RIGHT_F) > 60) {
						wifi.sendData(3);
						driveSystem.drive(0);
						driveSystem.turn90(-1.2f);
						withState = withStates.turn3;
					}
				}

				break;
				
			case turnIntoX2:

				if (irSens.sensGBackLeftBlack && irSens.sensGBackRightBlack) {
					driveSystem.drive(0);

					driveSystem.turn90(-1);
					withState = withStates.driveX2;

				}
				break;
				
			case driveX2:
				
				if (driveSystem.turn) {
					break;
				}
				
				
				driveLine();
				
				
				if (tofSens.getDistance(Const.SENSOR_BACK_L) > 265 
						&& tofSens.getDistance(Const.SENSOR_BACK_L) < 290
						|| tofSens.getDistance(Const.SENSOR_BACK_R) > 265
						&& tofSens.getDistance(Const.SENSOR_BACK_R) < 290) {
					
					if (tofSens.getDistance(Const.SENSOR_LEFT_B) > 60) {
						wifi.sendData(tofSens.getDistance(Const.SENSOR_RIGHT_B));
						wifi.sendData(11);
						driveSystem.drive(0);
						driveSystem.turn90(1);
						withState = withStates.turn3;
					}
				} else if (tofSens.getDistance(Const.SENSOR_BACK_L) > 170
						&& tofSens.getDistance(Const.SENSOR_BACK_L) < 190
						&& tofSens.getDistance(Const.SENSOR_BACK_R) > 170
						&& tofSens.getDistance(Const.SENSOR_BACK_R) < 190) {

					if (tofSens.getDistance(Const.SENSOR_LEFT_F) > 60) {
						wifi.sendData(22);
						driveSystem.drive(0);
						driveSystem.turn90(1);
						withState = withStates.turn3;
					}

				} else if (tofSens.getDistance(Const.SENSOR_BACK_L) > 330
						&& tofSens.getDistance(Const.SENSOR_BACK_L) < 360
						&& tofSens.getDistance(Const.SENSOR_BACK_R) > 330
						&& tofSens.getDistance(Const.SENSOR_BACK_R) < 360) {

					if (tofSens.getDistance(Const.SENSOR_LEFT_F) > 60) {
						wifi.sendData(33);
						driveSystem.drive(0);
						driveSystem.turn90(1);
						withState = withStates.turn3;
					}
				}

				break;
				
			case drive_X2:
				
				if (driveSystem.turn) {
					break;
				}
				
				driveLine();
				
				if (tofSens.getDistance(Const.SENSOR_FRONT_L) > 265
						&& tofSens.getDistance(Const.SENSOR_FRONT_L) < 290
						|| tofSens.getDistance(Const.SENSOR_FRONT_R) > 265
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) < 290) {
					
					if (tofSens.getDistance(Const.SENSOR_RIGHT_B) > 60) {
						wifi.sendData(111);
						driveSystem.drive(0);
						driveSystem.turn90(-1);
						withState = withStates.turn3;
					}
				} else if (tofSens.getDistance(Const.SENSOR_FRONT_L) > 90
						&& tofSens.getDistance(Const.SENSOR_FRONT_L) < 135
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) > 90
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) < 135) {

					if (tofSens.getDistance(Const.SENSOR_RIGHT_F) > 60) {
						wifi.sendData(222);
						driveSystem.drive(0);
						driveSystem.turn90(-1);
						withState = withStates.turn3;
					}

				} /*else if (tofSens.getDistance(Const.SENSOR_BACK_L) > 330
						&& tofSens.getDistance(Const.SENSOR_BACK_L) < 360
						&& tofSens.getDistance(Const.SENSOR_BACK_R) > 330
						&& tofSens.getDistance(Const.SENSOR_BACK_R) < 360) {

					if (tofSens.getDistance(Const.SENSOR_RIGHT_F) > 60) {
						wifi.sendData(333);
						driveSystem.drive(0);
						driveSystem.turn90(-1);
						withState = withStates.turn3;
					}
				}*/

				break;

			case turn3:
				if (!driveSystem.turn) {
					if (pos90Deg(3,4)) {
						driveSystem.drive(Const.MAX_DRIVE_SPEED);
						withState = withStates.driveY1;
					}
				}
				break;

			case driveY1:
				if (irSens.sensGFrontLeftBlack && irSens.sensGFrontRightBlack) {

					wifi.sendData(tofSens.getDistance(Const.SENSOR_FRONT_L));
					wifi.sendData(tofSens.getDistance(Const.SENSOR_FRONT_R));
					if (tofSens.getDistance(Const.SENSOR_FRONT_L) < 150
							|| tofSens.getDistance(Const.SENSOR_FRONT_R) < 150) {
						driveSystem.drive(10);
						withState = withStates.turnIntoX2;
					} else {
						wifi.sendData(00);
						withState = withStates.driveY1_5;
					}

				}
				break;

			case driveY1_2:
				if (irSens.sensGBackLeftBlack && irSens.sensGBackRightBlack) {
					if (tofSens.getDistance(Const.SENSOR_FRONT_L) < 100) {
						withState = withStates.turn7;
					}
				} else {
					withState = withStates.driveY1_5;
				}
				break;

			case turn4:
				// do s�ttter den fahra und a l�cka suacha
			
				driveSystem.drive(0);
				wifi.sendData(4);
				withState = withStates.stayPos1;
				break;

			case driveY1_3:
				if (irSens.sensGFrontLeftBlack && irSens.sensGFrontRightBlack) {
					driveSystem.drive(10);
					withState = withStates.driveY1_4;
				}
				break;

			case driveY1_4:
				if (irSens.sensGBackLeftBlack && irSens.sensGBackRightBlack) {
					driveSystem.drive(0);
					driveSystem.turn90(-1.2f);
					withState = withStates.turn5;
				}
				break;

			case driveY1_5:
				if (tofSens.getDistance(Const.SENSOR_BACK_L) > 500 
						&& tofSens.getDistance(Const.SENSOR_BACK_L) < 660
						|| tofSens.getDistance(Const.SENSOR_BACK_R) > 500
						&& tofSens.getDistance(Const.SENSOR_BACK_R) < 660) {
					if (irSens.sensGBackLeftBlack && irSens.sensGBackRightBlack) {
						wifi.sendData(7);
						driveSystem.turn90(-1.2f);
						withState = withStates.turn7;
					}
				} else if (tofSens.getDistance(Const.SENSOR_FRONT_L) > 310 && tofSens.getDistance(Const.SENSOR_FRONT_L) < 340
						|| tofSens.getDistance(Const.SENSOR_FRONT_R) > 310
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) < 340) {
					if (irSens.sensGBackLeftBlack && irSens.sensGBackRightBlack) {
						wifi.sendData(88);
						driveSystem.turn90(-1.2f);
						withState = withStates.turn7;
					}
				}

				break;

			case turn5:
				if (!driveSystem.turn) {
					driveSystem.drive(Const.MAX_DRIVE_SPEED);

				}
				break;
				
			case turn7:
				if (!driveSystem.turn) {
					if (pos90Deg(3, 4)) {
						withState = withStates.goToPos7;
					}
				}
				break;
				
			case goToPos7:
				driveLine();
				if (tofSens.getDistance(Const.SENSOR_FRONT_L) < 70
						|| tofSens.getDistance(Const.SENSOR_FRONT_R) < 70) {
					driveSystem.drive(0);
					driveSystem.turn90(-1.2f);
					shootSystem.setPosition(Const.POSITION_GET_BALL);
					withState = withStates.wait7;
				}
				break;
				
			case wait7:
				if (!shootSystem.inAction && !driveSystem.turn) {
					if (pos90Deg(7,8)) {
						wifi.sendData(Const.READY_POS7);
						withState = withStates.ballIn7;
					}
				}
				break;
				
			case ballIn7:
				driveSystem.drive(0);
				if (ballSens.getBallState() && wifi.getData() == Const.SHOOT_POS7) {
					wifi.sendData(Const.BALL_TRUE);
					driveSystem.turn90(-1);
					shootSystem.setPosition(Const.POSITION_SHOOT_HORIZONTAL);
					withState = withStates.shot8;
				}
				break;
				
			case shot8:
				if (!shootSystem.inAction && !driveSystem.turn) {
					if (pos90Deg(3,4)) {
						if (wifi.getData() == Const.READY_POS8) {
							wifi.sendData(Const.SHOOT_POS8);
							shootSystem.fire();
							shootSystem.setPosition(Const.POSITION_ZERO);
							withState = withStates.turn4;
							wifi.sendData(99);
							// PROGRAM END
						} 
					}
				}
				break;

//			case driveX2:
//				int actualDist = tofSens.getDistance(1);
//				int distDifference = oldDist - actualDist;
//				float desSpeed;
//				if (Math.abs(distDifference) < Const.DIST_POS_SIDE) {
//					driveSystem.drive(0);
//					driveSystem.turn90(-1);
//					shootSystem.setPosition(Const.POSITION_GET_BALL);
//					withState = withStates.turn6;
//				} else {
//					desSpeed = (float) (distDifference * Const.P_FAKTOR_DISTANCE);
//					if (desSpeed > Const.MAX_DRIVE_SPEED) {
//						desSpeed = Const.MAX_DRIVE_SPEED;
//					} else if (desSpeed < -Const.MAX_DRIVE_SPEED) {
//						desSpeed = -Const.MAX_DRIVE_SPEED;
//					}
//					driveSystem.drive(desSpeed);
//				}
//				break;

			case turn6:
				if (!driveSystem.turn && !shootSystem.inAction) {
					wifi.sendData(Const.READY_POS7);
					withState = withStates.wait2;
				}
				break;

			case wait2:
				break;

			}
			break;
			
// 			WITHOUTBALL
//----------------------------------------------------------------------------------------------
		case WITHOUTBALL:
			switch(withoutState) {
			case goToPos2:
				driveLine();
				
				if (tofSens.getDistance(Const.SENSOR_BACK_L) > 290
						&& tofSens.getDistance(Const.SENSOR_BACK_L) < 315
						&& tofSens.getDistance(Const.SENSOR_BACK_R) > 290
						&& tofSens.getDistance(Const.SENSOR_BACK_R) < 315) {
				
				withoutState = withoutStates.stopPos2;
				}
				break;
				
			case stopPos2:
				if (driveSystem.dist) {
					wifi.sendData(driveSystem.links());
				}
				
				if (tofSens.getDistance(Const.SENSOR_BACK_L) > 330
						&& tofSens.getDistance(Const.SENSOR_BACK_L) < 340
						&& tofSens.getDistance(Const.SENSOR_BACK_R) > 330
						&& tofSens.getDistance(Const.SENSOR_BACK_R) < 340) {
					
					driveSystem.drive(0);
					wifi.sendData(Const.READY_POS2);
					shootSystem.setPosition(Const.POSITION_GET_BALL);
					
					withoutState = withoutStates.wait3;
				}
				break;
				
			case wait3:
				if (ballSens.getBallState()) {
					shootSystem.setPosition(Const.POSITION_SHOOT_HORIZONTAL);
					withoutState = withoutStates.shot3;
				}
				break;
				
			case shot3:
				if (!shootSystem.inAction) {
					if (wifi.getData() == Const.READY_POS3) {
						wifi.sendData(Const.SHOOT_POS3);
						shootSystem.fire();
						shootSystem.setPosition(Const.POSITION_ZERO);
						wifi.sendData(Const.GOTO_POS6);
						withoutState = withoutStates.turnOn2;
					}
				}
				break;
				
			case turnOn2:
				if (driveSystem.turn) {
					break;
				}
				
				driveLine();
				
				if (tofSens.getDistance(Const.SENSOR_BACK_L) > 330
						&& tofSens.getDistance(Const.SENSOR_BACK_L) < 360
						&& tofSens.getDistance(Const.SENSOR_BACK_R) > 330
						&& tofSens.getDistance(Const.SENSOR_BACK_R) < 360) {
					if (tofSens.getDistance(Const.SENSOR_RIGHT_F) > 60) {
						driveSystem.drive(0);
						driveSystem.turn90(-1);
						withoutState = withoutStates.turn1;
					} else {
						driveSystem.turn180(-1);
						withoutState = withoutStates.drive_X1;
					}

				}
				break;
				
			case drive_X1:
				
				if (driveSystem.turn) {
					break;
				}
				
				driveLine();
				
				if (tofSens.getDistance(Const.SENSOR_FRONT_L) > 260
						&& tofSens.getDistance(Const.SENSOR_FRONT_L) < 280
						|| tofSens.getDistance(Const.SENSOR_FRONT_R) > 260
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) < 280) {
					
					if (tofSens.getDistance(Const.SENSOR_LEFT_F) > 60) {
						wifi.sendData(111);
						driveSystem.drive(0);
						driveSystem.turn90(1);
						withoutState = withoutStates.turn1;
					}
				} else if (tofSens.getDistance(Const.SENSOR_FRONT_L) > 90
						&& tofSens.getDistance(Const.SENSOR_FRONT_L) < 135
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) > 90
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) < 135) {

					if (tofSens.getDistance(Const.SENSOR_LEFT_F) > 60) {
						wifi.sendData(222);
						driveSystem.drive(0);
						driveSystem.turn90(1);
						withoutState = withoutStates.turn1;
					}

				}
				break;
				
			case turn1:
				if (!driveSystem.turn) {
					driveSystem.drive(Const.MAX_DRIVE_SPEED);
					withoutState = withoutStates.driveY1;
				}
				break;
			
			case driveY1:
				if (irSens.sensGFrontLeftBlack && irSens.sensGFrontRightBlack) {

					if (tofSens.getDistance(Const.SENSOR_FRONT_L) < 150
							|| tofSens.getDistance(Const.SENSOR_FRONT_R) < 150) {
						driveSystem.drive(10);
						withoutState = withoutStates.turn6;
					} else {
						
					driveSystem.drive(10);
					withoutState = withoutStates.turn6;
						
					}

				}
				break;	
							
			case turn6:
				if (tofSens.getDistance(Const.SENSOR_BACK_L) > 290
						|| tofSens.getDistance(Const.SENSOR_BACK_R) > 290) {
					wifi.sendData(77);
					driveSystem.drive(0);
					driveSystem.turn90(1);
					withoutState = withoutStates.goToPos6;
				}
				break;
				
			case goToPos6:
				if (!driveSystem.turn) {
					driveLine();
					if (tofSens.getDistance(Const.SENSOR_FRONT_L) < 70
							|| tofSens.getDistance(Const.SENSOR_FRONT_R) < 70) {
						driveSystem.drive(0);
						driveSystem.turn90(1);
						shootSystem.setPosition(Const.POSITION_GET_BALL);
						withoutState = withoutStates.wait6;
					}
				break;
				}
				
			case wait6:
				if (!driveSystem.turn) {
					wifi.sendData(Const.READY_POS6);
					withoutState = withoutStates.ballIn6;
					break;
				}
				
			case ballIn6:
				if (ballSens.getBallState() && (wifi.getData() == Const.SHOOT_POS6)) {
					wifi.sendData(Const.BALL_TRUE);
					driveSystem.turn180(1);
					withoutState = withoutStates.ready6;
				}
				break;
				
			case ready6:
				if (!driveSystem.turn) {
					shootSystem.setPosition(Const.POSITION_SHOOT_VERTICAL);
					withoutState = withoutStates.shot9;
				}
				break;
				
			case shot9:
				if (!shootSystem.inAction) {
					if (wifi.getData() == Const.READY_POS9) {
						wifi.sendData(Const.SHOOT_POS9);
						shootSystem.fire();
						shootSystem.setPosition(Const.POSITION_ZERO);
						wifi.sendData(Const.GOTO_POS8);
						driveSystem.turn90(-1);
						withoutState = withoutStates.driveX2;
					}
				}
				break;
				
			case driveX2:
				if (driveSystem.turn) {
					break;
				}
				
				driveLine();
							
				if (tofSens.getDistance(Const.SENSOR_BACK_L) > 260
						&& tofSens.getDistance(Const.SENSOR_BACK_L) < 280
						|| tofSens.getDistance(Const.SENSOR_BACK_R) > 260
						&& tofSens.getDistance(Const.SENSOR_BACK_R) < 280) {
					
					if (tofSens.getDistance(Const.SENSOR_LEFT_B) > 60) {
						wifi.sendData(1111);
						driveSystem.drive(0);
						driveSystem.turn90(1);
						withoutState = withoutStates.turn8;
					}
				}
				
				if (tofSens.getDistance(Const.SENSOR_FRONT_L) > 330
						&& tofSens.getDistance(Const.SENSOR_FRONT_L) < 360
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) > 330
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) < 360) {
					if (tofSens.getDistance(Const.SENSOR_LEFT_B) > 60) {
						wifi.sendData(2222);
						driveSystem.drive(0);
						driveSystem.turn90(1);
						withoutState = withoutStates.turn8;
					} 
				
				} else if (tofSens.getDistance(Const.SENSOR_FRONT_L) > 90
						&& tofSens.getDistance(Const.SENSOR_FRONT_L) < 110
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) > 90
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) < 110) {
					
					wifi.sendData(3333);
					driveSystem.drive(0);
					driveSystem.turn90(1);
					withoutState = withoutStates.turn8;
					}
	
				break;
				
			case turn8:
				if (!driveSystem.turn) {
					driveSystem.drive(Const.MAX_DRIVE_SPEED);
					withoutState = withoutStates.driveYFinal;			
				}
				break;
				
			case driveYFinal:
				if (irSens.sensGBackLeftBlack && irSens.sensGBackRightBlack) {
					driveSystem.drive(0);
					//TODO: go to final shot pos
				}
				break;
			}
				
			default:
				break;
			}
			
		}

	static {
		try {
// 1) Initialisiere SCI1 (9600 8N1)
			SCI sci1 = SCI.getInstance(SCI.pSCI1);

			sci1.start(9600, SCI.NO_PARITY, (short) 8);

// 2) Benutze SCI1 f�r Standard-Output
			System.out = new PrintStream(sci1.out);

			LeBotJames_newer_old t = new LeBotJames_newer_old();
			t.period = Const.MAIN_TASK_PERIOD;
			Task.install(t);
		} catch (Exception e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
