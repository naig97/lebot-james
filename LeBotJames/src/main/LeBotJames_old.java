package main;

import ch.ntb.inf.deep.runtime.ppc32.Task;
import ch.ntb.inf.deep.runtime.mpc555.driver.QADC_AIN;
import ch.ntb.inf.deep.runtime.mpc555.driver.SCI;
import java.io.PrintStream;

import def.Const;
import def.PinMap;
import comm.WiFi;
import shootSystem.ShootSystem;
import driveSystem.DriveSystem;
import ios.IO;
import sensors.IR;
import sensors.ToF;
import sensors.Ball;

public class LeBotJames_old extends Task {

	private DriveSystem driveSystem;
	private ShootSystem shootSystem;
	private WiFi wifi;
	private IR irSens;
	private ToF tofSens;
	private Ball ballSens;
	private IO ios;

	private boolean stateSWMode;
	private mainStates mainState;
	private conStates conState;
	private withStates withState;

	private int akkuBadValue;
	private boolean akkuOk;
	private boolean manuelMode;

	private int oldDist;

	public LeBotJames_old() throws Exception {

		driveSystem = new DriveSystem();
		shootSystem = new ShootSystem();
		wifi = new WiFi();
		irSens = new IR();
		tofSens = new ToF();
		ballSens = new Ball();
		ios = new IO();

		ios.pinInitState();
		driveSystem.drive(0);
		shootSystem.resetAngle();
		shootSystem.setPosition(Const.POSITION_GET_BALL);

		akkuOk = true;
		manuelMode = false;
		oldDist = 3000;

		mainState = mainStates.CONNECTION;
		conState = conStates.connect;
	}

	private static enum mainStates {
		CONNECTION, WITHBALL, WITHOUTBALL
	}

	private static enum conStates {
		connect, connectACK, ballPos1
	}

	private static enum withStates {
		stayPos1, wait1, shot1, getBall1, ballIn1, turn1, shot2, turn2, driveX1, turn3, driveY1, driveY1_2, turn4,
		driveY1_3, driveY1_4, driveY1_5, turn5, turn7, driveX2, turn6, wait2, goToPos7, readyPos7, wait7, ballIn7,
		shot8
	}

	public void action() {
		short akku = QADC_AIN.read(Const.USE_QADCA, PinMap.ADC_BATTERY);
		if (akku < akkuBadValue) { // Akku check
			if (akkuOk) {
				akkuBadValue += 3; // Hysteresis for voltage ripples
			}
			akkuOk = false;
			ios.ledBatteryBad.set(true);
		} else {
			akkuOk = true;
			akkuBadValue = Const.AKKU_BAD_VALUE;
			ios.ledBatteryBad.set(false);
		}

		switch (mainState) {

		// Connectivity check

		case CONNECTION:
			switch (conState) {
			case connect:
				if (wifi.getData() == Const.START) {
					wifi.sendData(Const.CONNECTION_CHECK);
					conState = conStates.connectACK;
				}
				break;

			case connectACK:
				if (wifi.getData() == Const.CONNECTION_OK) {
					wifi.sendData(Const.CONNECTION_OK);
					ios.ledWifi.set(false);
					shootSystem.driveFunnel();
					conState = conStates.ballPos1;
				}
				break;

			case ballPos1:
				if (!shootSystem.inAction) {
					if (ballSens.getBallState()) {// ios.switch1.get()) {
						wifi.sendData(Const.BALL_TRUE);
						shootSystem.setPosition(Const.POSITION_SHOOT_HORIZONTAL);
						mainState = mainStates.WITHBALL;
						withState = withStates.stayPos1;

					} else {
						wifi.sendData(Const.BALL_FALSE);
//						mainState = mainStates.WITHOUTBALL;
//						wifi.sendData(Const.BALL_FALSE);
//						mainState = mainStates.WITHBALL;
//						withState = withStates./*stayPos1*/turn1;
					}
				}
				break;
			}
			break;

		// start with ball
		// ------------------------------------------------------------------------------------------------

		case WITHBALL:
			switch (withState) {

			case stayPos1:
				if (!shootSystem.inAction) {
					withState = withStates.shot1;
				}
				break;

			case shot1:
				if (wifi.getData() == Const.READY_POS2) {
					wifi.sendData(Const.SHOOT_POS2);
					shootSystem.fire();
					shootSystem.setPosition(Const.POSITION_ZERO);
					withState = withStates.wait1;
				}
				break;

			case wait1:
				if (!shootSystem.inAction) {
					shootSystem.setPosition(Const.POSITION_GET_BALL);
					withState = withStates.getBall1;
				}
				break;

			case getBall1:
				if (!shootSystem.inAction) {
					wifi.sendData(Const.READY_POS1);
					withState = withStates.ballIn1;
				}
				break;

			case ballIn1:
				if (ballSens.getBallState() && (wifi.getData() == Const.SHOOT_POS1)) {
					wifi.sendData(Const.BALL_TRUE);
					withState = withStates.turn1;
				}
				break;

			case turn1:
				driveSystem.turn90(-1);
				shootSystem.setPosition(Const.POSITION_SHOOT_VERTICAL);
				withState = withStates.shot2;
				break;

			case shot2:
				if (!shootSystem.inAction && !driveSystem.turn) {
					// vieleicht noch genauere Ausrichtung mittels tof
					if (wifi.getData() == Const.READY_POS4) {
						wifi.sendData(Const.SHOOT_POS4);
						shootSystem.fire();
						shootSystem.setPosition(Const.POSITION_ZERO);
						wifi.sendData(Const.GOTO_POS7);
						driveSystem.turn90(1);
						withState = withStates.turn2;
					}
				}
				break;

			case turn2:
				if (!driveSystem.turn) {
					driveSystem.drive(Const.MAX_DRIVE_SPEED);
					withState = withStates.driveX1;
				}
				break;

			case driveX1:
				// wifi.sendData(tofSens.getDistance(Const.SENSOR_BACK_L));
				if (tofSens.getDistance(Const.SENSOR_BACK_L) > 265 && tofSens.getDistance(Const.SENSOR_BACK_L) < 290
						|| tofSens.getDistance(Const.SENSOR_BACK_R) > 265
								&& tofSens.getDistance(Const.SENSOR_BACK_R) < 290) {
					if (tofSens.getDistance(Const.SENSOR_RIGHT_B) > 60) {
						wifi.sendData(tofSens.getDistance(Const.SENSOR_RIGHT_B));
						wifi.sendData(1);
						driveSystem.drive(0);
						driveSystem.turn90(-1);
						withState = withStates.turn3;
					}
				} else if (tofSens.getDistance(Const.SENSOR_FRONT_L) > 170
						&& tofSens.getDistance(Const.SENSOR_FRONT_L) < 190
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) > 170
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) < 190) {

					if (tofSens.getDistance(Const.SENSOR_RIGHT_B) > 60) {
						wifi.sendData(2);
						driveSystem.drive(0);
						driveSystem.turn90(-1);
						withState = withStates.turn3;
					}

				} else if (tofSens.getDistance(Const.SENSOR_FRONT_L) > 100
						&& tofSens.getDistance(Const.SENSOR_FRONT_L) < 120
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) > 100
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) < 120) {

					if (tofSens.getDistance(Const.SENSOR_RIGHT_F) > 60) {
						wifi.sendData(3);
						driveSystem.drive(0);
						driveSystem.turn90(-1);
						withState = withStates.turn3;
					}
				}

				break;

			case turn3:
				if (!driveSystem.turn) {
					driveSystem.drive(Const.MAX_DRIVE_SPEED);
					withState = withStates.driveY1;
				}
				break;

			case driveY1:
				if (irSens.sensGFrontLeftBlack && irSens.sensGFrontRightBlack) {

					wifi.sendData(tofSens.getDistance(Const.SENSOR_FRONT_L));
					wifi.sendData(tofSens.getDistance(Const.SENSOR_FRONT_R));
					if (tofSens.getDistance(Const.SENSOR_FRONT_L) < 200
							|| tofSens.getDistance(Const.SENSOR_FRONT_R) < 200) {
						driveSystem.drive(10);
						withState = withStates.driveY1_2;
					} else {
						withState = withStates.driveY1_5;
					}

				}
				break;

			case driveY1_2:
				if (irSens.sensGBackLeftBlack && irSens.sensGBackRightBlack) {
					if (tofSens.getDistance(Const.SENSOR_FRONT_L) < 100) {
						withState = withStates.turn7;
					}
				} else {
					withState = withStates.driveY1_5;
				}
				break;

			case turn4:
				// do s�ttter den fahra und a l�cka suacha
			
				driveSystem.drive(0);
				wifi.sendData(4);
				break;

			case driveY1_3:
				if (irSens.sensGFrontLeftBlack && irSens.sensGFrontRightBlack) {
					driveSystem.drive(10);
					withState = withStates.driveY1_4;
				}
				break;

			case driveY1_4:
				if (irSens.sensGBackLeftBlack && irSens.sensGBackRightBlack) {
					driveSystem.drive(0);
					driveSystem.turn90(-1);
					withState = withStates.turn5;
				}
				break;

			case driveY1_5:
				if (tofSens.getDistance(Const.SENSOR_BACK_L) > 500 && tofSens.getDistance(Const.SENSOR_BACK_L) < 560
						|| tofSens.getDistance(Const.SENSOR_BACK_R) > 500
								&& tofSens.getDistance(Const.SENSOR_BACK_R) < 560) {
					if (irSens.sensGBackLeftBlack && irSens.sensGBackRightBlack) {
						wifi.sendData(7);
						driveSystem.turn90(-1);
						withState = withStates.turn7;
					}
				} else if (tofSens.getDistance(Const.SENSOR_FRONT_L) > 310 && tofSens.getDistance(Const.SENSOR_FRONT_L) < 340
						|| tofSens.getDistance(Const.SENSOR_FRONT_R) > 310
						&& tofSens.getDistance(Const.SENSOR_FRONT_R) < 340) {
					if (irSens.sensGBackLeftBlack && irSens.sensGBackRightBlack) {
						wifi.sendData(88);
						driveSystem.turn90(-1);
						withState = withStates.turn7;
					}
				}

				break;

			case turn5:
				if (!driveSystem.turn) {
					driveSystem.drive(Const.MAX_DRIVE_SPEED);

				}
				break;
				
			case turn7:
				if (!driveSystem.turn) {	
					driveSystem.drive(Const.MAX_DRIVE_SPEED);
					withState = withStates.goToPos7;
				}
				break;
				
			case goToPos7:
				if (tofSens.getDistance(Const.SENSOR_FRONT_L) < 70
						|| tofSens.getDistance(Const.SENSOR_FRONT_R) < 70) {
					driveSystem.drive(0);
					driveSystem.turn90(-1);
					shootSystem.setPosition(Const.POSITION_GET_BALL);
					withState = withStates.wait7;
				}
				break;
				
			case wait7:
				if (!shootSystem.inAction) {
					wifi.sendData(Const.READY_POS7);
					withState = withStates.ballIn7;
				}
				break;
				
			case ballIn7:
				if (ballSens.getBallState()) {
					wifi.sendData(Const.BALL_TRUE);
					driveSystem.turn90(-1);
					shootSystem.setPosition(Const.POSITION_SHOOT_HORIZONTAL);
					withState = withStates.shot8;
				}
				break;
				
			case shot8:
				if (!shootSystem.inAction && !driveSystem.turn) {
					// vieleicht noch genauere Ausrichtung mittels tof
					if (wifi.getData() == Const.READY_POS8) {
						wifi.sendData(Const.SHOOT_POS8);
						shootSystem.fire();
						shootSystem.setPosition(Const.POSITION_ZERO);
						withState = withStates.turn4;
						wifi.sendData(99);
						// PROGRAM END
					} 
				}
				break;

			case driveX2:
				int actualDist = tofSens.getDistance(1);
				int distDifference = oldDist - actualDist;
				float desSpeed;
				if (Math.abs(distDifference) < Const.DIST_POS_SIDE) {
					driveSystem.drive(0);
					driveSystem.turn90(-1);
					shootSystem.setPosition(Const.POSITION_GET_BALL);
					withState = withStates.turn6;
				} else {
					desSpeed = (float) (distDifference * Const.P_FAKTOR_DISTANCE);
					if (desSpeed > Const.MAX_DRIVE_SPEED) {
						desSpeed = Const.MAX_DRIVE_SPEED;
					} else if (desSpeed < -Const.MAX_DRIVE_SPEED) {
						desSpeed = -Const.MAX_DRIVE_SPEED;
					}
					driveSystem.drive(desSpeed);
				}
				break;

			case turn6:
				if (!driveSystem.turn && !shootSystem.inAction) {
					wifi.sendData(Const.READY_POS7);
					withState = withStates.wait2;
				}
				break;

			case wait2:
				break;

			}
			break;

		case WITHOUTBALL:
			break;

		}
	}

	static {
		try {
// 1) Initialisiere SCI1 (9600 8N1)
			SCI sci1 = SCI.getInstance(SCI.pSCI1);

			sci1.start(9600, SCI.NO_PARITY, (short) 8);

// 2) Benutze SCI1 f�r Standard-Output
			System.out = new PrintStream(sci1.out);

			LeBotJames_old t = new LeBotJames_old();
			t.period = Const.MAIN_TASK_PERIOD;
			Task.install(t);
		} catch (Exception e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
