package main;

import ch.ntb.inf.deep.runtime.ppc32.Task;
import ch.ntb.inf.deep.runtime.mpc555.driver.QADC_AIN;
import ch.ntb.inf.deep.runtime.mpc555.driver.SCI;
import java.io.PrintStream;

import def.Const;
import def.PinMap;
import comm.WiFi;
import shootSystem.ShootSystem;
import driveSystem.DriveSystem;
import ios.IO;
import sensors.IR;
import sensors.ToF;
import sensors.Ball;

public class LeBotJames extends Task {

	private DriveSystem driveSystem;
	private ShootSystem shootSystem;
	private WiFi wifi;
	private IR irSens;
	private ToF tofSens;
	private Ball ballSens;
	private IO ios;

	private boolean stateSWMode;
	private mainStates mainState;
	private conStates conState;
	private withStates withState;
	private withoutStates withoutState;

	private int akkuBadValue;
	private boolean akkuOk;
	private boolean manuelMode;

	private int oldDist;

	public LeBotJames() throws Exception {

		driveSystem = new DriveSystem();
		shootSystem = new ShootSystem();
		wifi = new WiFi();
		irSens = new IR();
		tofSens = new ToF();
		ballSens = new Ball();
		ios = new IO();

		ios.pinInitState();
		driveSystem.drive(0);
		shootSystem.resetAngle();
		shootSystem.setPosition(Const.POSITION_GET_BALL);

		akkuOk = true;
		manuelMode = false;
		oldDist = 3000;

		mainState = mainStates.CONNECTION;
		conState = conStates.connect;
	}

	private static enum mainStates {
		CONNECTION, WITHBALL, WITHOUTBALL
	}

	private static enum conStates {
		connect, connectACK, ballPos1
	}

	private static enum withStates {
		stayPos1, wait1, shot1, getBall1, ballIn1, turn1, shot2, turn2, driveX1, driveY1, driveY1_4,
		turn7, turn7_1, driveX2, getBall7, ballIn7,shot8, driveX2_0, driveX2_1, driveX2_11, driveX2_2,
		driveX2_3, driveY2, driveY2_1, driveX3_1, driveX3_2, driveX3_3, driveX2_33, end
	}

	private static enum withoutStates {
		goToPos2, shot3, ballIn2, drive_X1, drive_X2_1, drive_X2_2, drive_X2_3, drive_X2, drive_X3, drive_X4,
		drive_X4_1, drive_X4_2, drive_X4_3, drive_Y3, turn1, turn1_2, driveY1,stopPos2, turn6, wait6,
		ballIn6, ready6, shot9, turn8, ready8, ready8_1, ballIn8, turnOn2, readyFinal, finalShot, end
	}
	// FIXME: nur dasi nid immer ufascrolla muas

	private void driveLine() {
		if (irSens.sensGFrontRightBlack && irSens.sensGFrontLeftBlack) {
			driveSystem.drive(Const.MAX_DRIVE_SPEED);
		} else if (irSens.sensGFrontRightBlack) {
			driveSystem.driveSingle(Const.MAX_DRIVE_SPEED - 10, Const.MAX_DRIVE_SPEED);
		} else if (irSens.sensGFrontLeftBlack) {
			driveSystem.driveSingle(Const.MAX_DRIVE_SPEED, Const.MAX_DRIVE_SPEED - 10);
		} else {
			driveSystem.drive(Const.MAX_DRIVE_SPEED);
		}
	}

	private boolean pos90Deg(int sens1, int sens2) {
		int difference = tofSens.getDistance(sens1) - tofSens.getDistance(sens2);
		wifi.sendData(difference);
		if (Math.abs(difference) >= 1) {
			if (difference < 0) {
				driveSystem.driveSingle(-3, 3);
			} else {
				driveSystem.driveSingle(3, -3);
			}
			return false;
		} else {
			return true;
		}
	}

	public void action() {
		short akku = QADC_AIN.read(Const.USE_QADCA, PinMap.ADC_BATTERY);
		if (akku < akkuBadValue) { // Akku check
			if (akkuOk) {
				akkuBadValue += 3; // Hysteresis for voltage ripples
			}
			akkuOk = false;
			ios.ledBatteryBad.set(true);
		} else {
			akkuOk = true;
			akkuBadValue = Const.AKKU_BAD_VALUE;
			ios.ledBatteryBad.set(false);
		}

		switch (mainState) {

		// Connectivity check

		case CONNECTION:
			switch (conState) {
			case connect:
				if (wifi.getData() == Const.START) {
					wifi.sendData(Const.CONNECTION_CHECK);
					conState = conStates.connectACK;
				}
				break;

			case connectACK:
				if (wifi.getData() == Const.CONNECTION_OK) {
					wifi.sendData(Const.CONNECTION_OK);
					ios.ledWifi.set(false);
					shootSystem.driveFunnel();
					conState = conStates.ballPos1;
				}
				break;

			case ballPos1:
				if (!shootSystem.inAction) {
					if (ballSens.getBallState()) {
						wifi.sendData(Const.BALL_TRUE);
						shootSystem.setPosition(Const.POSITION_SHOOT_HORIZONTAL2);
						mainState = mainStates.WITHBALL;
						withState = withStates.stayPos1;
						

					} else if (wifi.getData() == Const.BALL_TRUE) {
						wifi.sendData(Const.BALL_FALSE);
						mainState = mainStates.WITHOUTBALL;
						withoutState = withoutStates.goToPos2;
						wifi.sendData(Const.GOTO_POS2);
					}
				}
				break;
			}
			break;

		// start with ball
		// ------------------------------------------------------------------------------------------------

		case WITHBALL:
			switch (withState) {

			case stayPos1:
				if (!shootSystem.inAction) {
					withState = withStates.shot1;
				}
				break;

			case shot1:
				if (wifi.getData() == Const.READY_POS2) {
					wifi.sendData(Const.SHOOT_POS2);
					shootSystem.fire();
					shootSystem.setPosition(Const.POSITION_ZERO);
					withState = withStates.wait1;
				}
				break;

			case wait1:
				if (!shootSystem.inAction) {
					shootSystem.setPosition(Const.POSITION_GET_BALL);
					withState = withStates.getBall1;
				}
				break;

			case getBall1:
				if (!shootSystem.inAction) {
					wifi.sendData(Const.READY_POS1);
					withState = withStates.ballIn1;
				}
				break;

			case ballIn1:
				if (ballSens.getBallState() && (wifi.getData() == Const.SHOOT_POS1)) {
					wifi.sendData(Const.BALL_TRUE);
					withState = withStates.turn1;
				}
				break;

			case turn1:
				driveSystem.turn90(-1);
				shootSystem.setPosition(Const.POSITION_SHOOT_VERTICAL);
				withState = withStates.shot2;
				break;

			case shot2:
				if (!shootSystem.inAction && !driveSystem.turn) {
					if (wifi.getData() == Const.READY_POS4) {
						wifi.sendData(Const.SHOOT_POS4);
						shootSystem.fire();
						shootSystem.setPosition(Const.POSITION_ZERO);
						wifi.sendData(Const.GOTO_POS7);
						driveSystem.turn90(1);
						withState = withStates.turn2;
					}
				}
				break;

			case turn2:
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 220f);
					withState = withStates.driveX1;
				}
				break;

			case driveX1:				
				if (!driveSystem.dist) {
					if (tofSens.getDistance(Const.SENSOR_FRONT_L) < 20 
							|| tofSens.getDistance(Const.SENSOR_FRONT_L) < 20) {
						driveSystem.turn90(-1);
						withState = withStates.driveY1;
					} else if (tofSens.getDistance(Const.SENSOR_RIGHT_B) > 60) {
						driveSystem.turn90(-1);
						withState = withStates.driveY1;
					} else {
						driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 162f);
						}
					}
				break;
				
			case driveY1:
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 261f);
					withState = withStates.driveX2;
				}
				break;
				
			case driveY2_1:
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 261f);
					withState = withStates.driveY2;
				}
				break;
				
			case driveX2:
				if (!driveSystem.dist) {
//					wifi.sendData(55);
					if (tofSens.getDistance(Const.SENSOR_FRONT_R) > 60) {
						driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 261);
						withState = withStates.driveY2;
					} else {
						withState = withStates.driveX2_0;
						}
				}
				break;
				
				
				
			case driveX2_0:
				if (!driveSystem.dist && !driveSystem.turn) {
//					wifi.sendData(66);
					if (tofSens.getDistance(Const.SENSOR_LEFT_B) > 0 && tofSens.getDistance(Const.SENSOR_LEFT_B) < 80 
							|| (tofSens.getDistance(Const.SENSOR_LEFT_F) > 0 && tofSens.getDistance(Const.SENSOR_LEFT_F) < 80)) {
//						wifi.sendData(111);
						driveSystem.turn90(-1);
						withState = withStates.driveX2_1;
						
					} else if (tofSens.getDistance(Const.SENSOR_LEFT_B) > 160 && tofSens.getDistance(Const.SENSOR_LEFT_B) < 240 
							|| (tofSens.getDistance(Const.SENSOR_LEFT_F) > 160 && tofSens.getDistance(Const.SENSOR_LEFT_F) < 240)) {
//						wifi.sendData(112);
						driveSystem.turn90(-1);
						withState = withStates.driveX2_3;
						
					} else if (tofSens.getDistance(Const.SENSOR_LEFT_B) > 310 && tofSens.getDistance(Const.SENSOR_LEFT_B) < 390 
							|| (tofSens.getDistance(Const.SENSOR_LEFT_F) > 310 && tofSens.getDistance(Const.SENSOR_LEFT_F) < 390)) {
//						wifi.sendData(113);
						driveSystem.turn90(1);
						
						withState = withStates.driveX2_2;
						//withState = withStates.end;
					}
				}
				break;
				
			case driveX2_1:
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 125f);
					withState = withStates.driveX2_11;
				}
				break;

			case driveX2_11:				
				if (!driveSystem.dist) {
					if (tofSens.getDistance(Const.SENSOR_LEFT_B) > 60) {
						driveSystem.turn90(1);
						withState = withStates.driveY2_1;
					} else {
						driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 130f);
						}
					}
				break;
				
			case driveX2_2:				
				if (tofSens.getDistance(Const.SENSOR_FRONT_L) < 20 
						|| tofSens.getDistance(Const.SENSOR_FRONT_L) < 20) {
					driveSystem.turn90(-1);
					withState = withStates.driveY1;
				} else if (!driveSystem.dist && !driveSystem.turn) {
					if (tofSens.getDistance(Const.SENSOR_RIGHT_B) > 60) {
						driveSystem.turn90(-1);
						withState = withStates.driveY2_1;
					} else {
						driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 162f);
						}
					}
				break;
				
			case driveX2_3:
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 160f);
					withState = withStates.driveX2_33;
				}
				break;
				
			case driveX2_33:				
				if (!driveSystem.dist) {
//					wifi.sendData(44);
						driveSystem.turn90(1);
						withState = withStates.driveX2_0;
					}
				break;
				
			case driveY2:
				if (!driveSystem.dist) {
//					wifi.sendData(tofSens.getDistance(Const.SENSOR_LEFT_B));
					if (tofSens.getDistance(Const.SENSOR_LEFT_B) > 0 && tofSens.getDistance(Const.SENSOR_LEFT_B) < 80 
							|| (tofSens.getDistance(Const.SENSOR_LEFT_F) > 0 && tofSens.getDistance(Const.SENSOR_LEFT_F) < 80)) {
						driveSystem.turn90(-1);
						withState = withStates.driveX3_3;
						
					} else if (tofSens.getDistance(Const.SENSOR_LEFT_B) > 160 && tofSens.getDistance(Const.SENSOR_LEFT_B) < 240 
							|| (tofSens.getDistance(Const.SENSOR_LEFT_F) > 160 && tofSens.getDistance(Const.SENSOR_LEFT_F) < 240)) {
						driveSystem.turn90(-1);
						withState = withStates.driveX3_2;
						
					} else if (tofSens.getDistance(Const.SENSOR_LEFT_B) > 320 && tofSens.getDistance(Const.SENSOR_LEFT_B) < 390 
							|| (tofSens.getDistance(Const.SENSOR_LEFT_F) > 320 && tofSens.getDistance(Const.SENSOR_LEFT_F) < 390)) {
						driveSystem.turn90(-1);
						withState = withStates.driveX3_1;
					}
				}
				break;
				
			case driveX3_1:
//				wifi.sendData(21);
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 190f);
					withState = withStates.turn7;
				}
				break;
				
			case driveX3_2:
//				wifi.sendData(22);
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 360f);
					withState = withStates.turn7;
				}
				break;
				
			case driveX3_3:
//				wifi.sendData(23);
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 528f);
					withState = withStates.turn7;
				}
				break;		

			case turn7:
				if (!driveSystem.dist) {
					driveSystem.turn90(-1);
					shootSystem.setPosition(Const.POSITION_GET_BALL);
					withState = withStates.getBall7;
					}
				break;
				
			case getBall7:
				if (!driveSystem.turn && !shootSystem.inAction) {
					wifi.sendData(Const.READY_POS7);
					withState = withStates.ballIn7;
				}
				break;
				
			case ballIn7:
				if (ballSens.getBallState() && (wifi.getData() == Const.SHOOT_POS7)) {
					wifi.sendData(Const.BALL_TRUE);
					driveSystem.turn90(-1);
					withState = withStates.turn7_1;
				}
				break;
				
			case turn7_1:
				if (!driveSystem.turn && wifi.getData() == Const.READY_POS8) {
					shootSystem.setPosition(Const.POSITION_SHOOT_HORIZONTAL2);
					withState = withStates.shot8;
				}
				break;
				
			case shot8:
				if (!shootSystem.inAction) {
					wifi.sendData(Const.SHOOT_POS8);
					shootSystem.fire();
					shootSystem.setPosition(Const.POSITION_ZERO);
					withState = withStates.end;
					
				}
				break;

			case end:
				break;

			}
			break;

// 			WITHOUTBALL
//----------------------------------------------------------------------------------------------
		case WITHOUTBALL:
			switch (withoutState) {

			case goToPos2:
				driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 280f);
				withoutState = withoutStates.stopPos2;
				break;

			case stopPos2:
				if (!driveSystem.dist) {
					wifi.sendData(Const.READY_POS2);
					shootSystem.setPosition(Const.POSITION_GET_BALL);
					withoutState = withoutStates.ballIn2;
				}
				break;

			case ballIn2:
				if (ballSens.getBallState()) {
					wifi.sendData(Const.BALL_TRUE);
					shootSystem.setPosition(Const.POSITION_SHOOT_HORIZONTAL);
					withoutState = withoutStates.shot3;
				}
				break;

			case shot3:
				if (!shootSystem.inAction) {
					if (wifi.getData() == Const.READY_POS3) {
						wifi.sendData(Const.SHOOT_POS3);
						shootSystem.fire();
						shootSystem.setPosition(Const.POSITION_ZERO);
						wifi.sendData(Const.GOTO_POS6);
						withoutState = withoutStates.turnOn2;
					}
				}
				break;

			case turnOn2:

				if (tofSens.getDistance(Const.SENSOR_RIGHT_F) > 60) {
					driveSystem.drive(0);
					driveSystem.turn90(-1);
					withoutState = withoutStates.turn1;
				} else {
					driveSystem.turn180(-1);
					withoutState = withoutStates.turn1_2;
				}

				break;

			case turn1_2:
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 155f);
					withoutState = withoutStates.drive_X1;
				}
				break;

			case turn1:
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 261);
					withoutState = withoutStates.driveY1;
				}
				break;

			case drive_X1:

				if (!driveSystem.dist) {
					if (tofSens.getDistance(Const.SENSOR_LEFT_B) > 60) {
						driveSystem.turn90(1);
						withoutState = withoutStates.turn1;
					} else {
						driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 120f);
					}
				}
				break;

			case driveY1:
				if (!driveSystem.dist) {
					if (tofSens.getDistance(Const.SENSOR_RIGHT_B) > 55 && tofSens.getDistance(Const.SENSOR_RIGHT_B) < 80
							|| (tofSens.getDistance(Const.SENSOR_RIGHT_F) > 55
									&& tofSens.getDistance(Const.SENSOR_RIGHT_F) < 80)) {
						driveSystem.turn90(1);
						withoutState = withoutStates.drive_X2_3;

					} else if (tofSens.getDistance(Const.SENSOR_RIGHT_B) > 170
							&& tofSens.getDistance(Const.SENSOR_RIGHT_B) < 190
							|| (tofSens.getDistance(Const.SENSOR_RIGHT_F) > 170
									&& tofSens.getDistance(Const.SENSOR_RIGHT_F) < 190)) {
						driveSystem.turn90(1);
						withoutState = withoutStates.drive_X2_2;

					} else if (tofSens.getDistance(Const.SENSOR_LEFT_B) > 260
							&& tofSens.getDistance(Const.SENSOR_LEFT_B) < 300
							|| (tofSens.getDistance(Const.SENSOR_LEFT_F) > 260
									&& tofSens.getDistance(Const.SENSOR_LEFT_F) < 390)) {
						driveSystem.turn90(1);
						withoutState = withoutStates.drive_X2_1;
					}
				}
				break;

			case drive_X2_1:
				//wifi.sendData(21);
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 230f);
					withoutState = withoutStates.turn6;
				}
				break;

			case drive_X2_2:
				//wifi.sendData(22);
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 370f);
					withoutState = withoutStates.turn6;
				}
				break;

			case drive_X2_3:
				//wifi.sendData(23);
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 500f);
					withoutState = withoutStates.turn6;
				}
				break;

			case turn6:
				if (!driveSystem.dist) {
					driveSystem.turn90(1);
					shootSystem.setPosition(Const.POSITION_GET_BALL);
					withoutState = withoutStates.wait6;
				}
				break;

			case wait6:
				if (!driveSystem.turn && !shootSystem.inAction) {
					wifi.sendData(Const.READY_POS6);
					withoutState = withoutStates.ballIn6;
				}
				break;

			case ballIn6:
				if (ballSens.getBallState() && (wifi.getData() == Const.SHOOT_POS6)) {
					wifi.sendData(Const.BALL_TRUE);
					shootSystem.setPosition(Const.POSITION_ZERO);
					driveSystem.turn180(1);
					withoutState = withoutStates.ready6;
				}
				break;

			case ready6:
				if (!driveSystem.turn && wifi.getData() == Const.READY_POS9) {
					shootSystem.setPosition(Const.POSITION_SHOOT_VERTICAL);
					withoutState = withoutStates.shot9;
				}
				break;

			case shot9:
				if (!shootSystem.inAction) {
					if (wifi.getData() == Const.READY_POS9) {
						wifi.sendData(Const.SHOOT_POS9);
						shootSystem.fire();
						shootSystem.setPosition(Const.POSITION_ZERO);
						wifi.sendData(Const.GOTO_POS8);
						driveSystem.turn90(-1);
						withoutState = withoutStates.drive_X2;
					}
				}
				break;

			case drive_X2:
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 215f);
					withoutState = withoutStates.drive_X3;
				}
				break;

			case drive_X3:
				if (!driveSystem.dist) {
					if (tofSens.getDistance(Const.SENSOR_LEFT_B) > 60) {
						driveSystem.turn90(1);
						withoutState = withoutStates.drive_Y3;
					} else {
						driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 157.5f);
					}
				}
				break;

			case drive_Y3:
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 261);
					withoutState = withoutStates.drive_X4;
				}
				break;

			case drive_X4:
				if (!driveSystem.dist) {
					if (tofSens.getDistance(Const.SENSOR_RIGHT_B) > 0 && tofSens.getDistance(Const.SENSOR_RIGHT_B) < 40
							|| (tofSens.getDistance(Const.SENSOR_RIGHT_F) > 0
									&& tofSens.getDistance(Const.SENSOR_RIGHT_F) < 40)) {
						driveSystem.turn90(1);
						withoutState = withoutStates.drive_X4_3;

					} else if (tofSens.getDistance(Const.SENSOR_RIGHT_B) > 170
							&& tofSens.getDistance(Const.SENSOR_RIGHT_B) < 190
							|| (tofSens.getDistance(Const.SENSOR_RIGHT_F) > 170
									&& tofSens.getDistance(Const.SENSOR_RIGHT_F) < 190)) {
						driveSystem.turn90(1);
						withoutState = withoutStates.drive_X4_2;

					} else if (tofSens.getDistance(Const.SENSOR_RIGHT_B) > 330
							&& tofSens.getDistance(Const.SENSOR_RIGHT_B) < 360
							|| (tofSens.getDistance(Const.SENSOR_RIGHT_F) > 330
									&& tofSens.getDistance(Const.SENSOR_RIGHT_F) < 360)) {
						driveSystem.turn90(-1);
						withoutState = withoutStates.drive_X4_1;
					}
				}
				break;

			case drive_X4_1:
				//wifi.sendData(41);
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 55f);
					withoutState = withoutStates.turn8;
				}
				break;

			case drive_X4_2:
				//wifi.sendData(42);
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 130f);
					shootSystem.setPosition(Const.POSITION_GET_BALL);
					withoutState = withoutStates.ready8;
				}
				break;

			case drive_X4_3:
				//wifi.sendData(43);
				if (!driveSystem.turn) {
					driveSystem.driveDist(Const.MAX_DRIVE_SPEED, 260f);
					shootSystem.setPosition(Const.POSITION_GET_BALL);
					withoutState = withoutStates.ready8;
				}
				break;

			case turn8:
				if (!driveSystem.dist) {
					driveSystem.turn180(1);
					shootSystem.setPosition(Const.POSITION_GET_BALL);
					withoutState = withoutStates.ready8;
				}
				break;

			case ready8:
				if (!shootSystem.inAction) {
					wifi.sendData(Const.READY_POS8);
					withoutState = withoutStates.ballIn8;
				}
				break;

			case ready8_1:
				if (!driveSystem.turn && !shootSystem.inAction) {
					wifi.sendData(Const.READY_POS8);
					withoutState = withoutStates.ballIn8;
				}
				break;

			case ballIn8:
				if (ballSens.getBallState() && (wifi.getData() == Const.SHOOT_POS8)) {
					wifi.sendData(Const.BALL_TRUE);
					driveSystem.turn90(-1);
					withoutState = withoutStates.readyFinal;
				}
				break;

			case readyFinal:
				if (!driveSystem.turn) {
					shootSystem.setPosition(Const.POSITION_SHOOT_BASKET);
					withoutState = withoutStates.finalShot;
				}
				break;

			case finalShot:
				if (!shootSystem.inAction) {
					wifi.sendData(Const.SHOOT_FINAL);
					shootSystem.fire();
					shootSystem.setPosition(Const.POSITION_ZERO);
					withoutState = withoutStates.end;
				}
				
			case end:
				break;
			}

		default:
			break;
		}

	}

	static {
		try {
			/*
			 * // 1) Initialisiere SCI1 (9600 8N1) SCI sci1 = SCI.getInstance(SCI.pSCI1);
			 * 
			 * sci1.start(9600, SCI.NO_PARITY, (short) 8);
			 * 
			 * // 2) Benutze SCI1 f�r Standard-Output System.out = new
			 * PrintStream(sci1.out);
			 */
			LeBotJames t = new LeBotJames();
			t.period = Const.MAIN_TASK_PERIOD;
			Task.install(t);
		} catch (Exception e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
