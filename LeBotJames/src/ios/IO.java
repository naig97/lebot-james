package ios;


import def.*;
import ch.ntb.inf.deep.runtime.mpc555.driver.TPU_DIO;
import ch.ntb.inf.deep.runtime.mpc555.driver.MPIOSM_DIO;
import ch.ntb.inf.deep.runtime.mpc555.driver.QADC_AIN;

public class IO {
	
	public TPU_DIO led1, led2, led3;
	public TPU_DIO switch1, switch2;
	public MPIOSM_DIO ledWifi, ledBatteryBad, ledManualOn;
	public MPIOSM_DIO groundEn, groundB, groundA;
	public MPIOSM_DIO manualDir, manualSet;
	public MPIOSM_DIO liftingMagnet, chargeC, electricMagnet, wifiReset;
	
	public IO() {
		// initialize debug LEDs (output)
		led1 = new TPU_DIO(true, PinMap.DEBUG_LED1, true);
		led2 = new TPU_DIO(true, PinMap.DEBUG_LED2, true);
		led3 = new TPU_DIO(true, PinMap.DEBUG_LED3, true);
		
		// initialize debug switches (input)
		switch1 = new TPU_DIO(true, PinMap.DEBUG_SWITCH1, false);
		switch2 = new TPU_DIO(true, PinMap.DEBUG_SWITCH2, false);
		
		// initialize status LEDs (output)
		ledWifi = new MPIOSM_DIO(PinMap.LED_STATUS_WIFI, true);
		ledBatteryBad = new MPIOSM_DIO(PinMap.LED_STATUS_BATTERY_BAD, true);
		ledManualOn = new MPIOSM_DIO(PinMap.LED_STATUS_MANUAL_MODE_ON, true);
		
		// initialize ground sensors (output)
		groundEn = new MPIOSM_DIO(PinMap.GROUND_SENSOR_ENABLE, true);
		groundB = new MPIOSM_DIO(PinMap.GROUND_SENSOR_B, true);
		groundA = new MPIOSM_DIO(PinMap.GROUND_SENSOR_A , true);
		
		// initialize manual modus switches (input)
		manualDir = new MPIOSM_DIO(PinMap.MANUAL_SWITCH_DIRECTION, false);
		manualSet = new MPIOSM_DIO(PinMap.MANUAL_SWITCH_DIRECTION_SET, false);
		
		// initialize various outputs
		liftingMagnet = new MPIOSM_DIO(PinMap.LIFTING_MAGNET, true);
		chargeC = new MPIOSM_DIO(PinMap.CHARGE_CONDENSATOR, true);
		electricMagnet = new MPIOSM_DIO(PinMap.CHARGE_CONDENSATOR, true);
		
		// initialize reset pin for WLAN
		//wifiReset = new MPIOSM_DIO(PinMap.WIFI_RESET, true);
		
		// initialize adc inputs for ir-sensors
		QADC_AIN.init(Const.USE_QADCA);
	}
	
	public void pinInitState() {
		// debug LEDs
		led1.set(false);
		led2.set(false);
		led3.set(false);
		
		// status LEDs
		ledWifi.set(true);
		ledBatteryBad.set(false);
		ledManualOn.set(false);
		
		// ground sensors 
		groundEn.set(false);
		groundB.set(false);
		groundA.set(false);
		
		// stuffy McStuffface
		liftingMagnet.set(false);
		chargeC.set(true);
		electricMagnet.set(false);
	}
}

